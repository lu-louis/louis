package converter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class Object_And_Byte implements Cloneable 
{
	byte[] yourBytes = null;
	
	Object yourObject = null;
	
	public Object_And_Byte(){ /****/ }
	
	public byte[] Object_To_Byte(Object o) throws IOException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(bos); 
		
		out.writeObject(o);
		out.flush();
		
		this.yourBytes = bos.toByteArray();
		
		out.close();
		bos.close();
		
		return this.yourBytes;
	}
	
	public Object Byte_To_Object(byte[] b) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(b);
		ObjectInput in = new ObjectInputStream(bis);
		
		this.yourObject =  in.readObject();
		
		in.close();
		bis.close();
		
		return this.yourObject;
	}

}
