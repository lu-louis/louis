package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
 
public class UDP_client {
 
	private String Host = null;
	private int Port = 0;
	
	private DatagramPacket dp = null;
	private DatagramSocket ds = null;
	
	public UDP_client(String _Host, int _Port) throws SocketException 
    {
		this.Host = _Host;
		this.Port = _Port;
    	this.ds = new DatagramSocket();
    }
	
    public void  sent(byte[] msg) throws IOException 
    {    	
    	this.dp = new DatagramPacket(msg, msg.length, InetAddress.getByName(this.Host), this.Port);
    	this.ds.send(this.dp);
    } 	
	
    public void  close()
    { 
    	this.ds.close();
    }

}