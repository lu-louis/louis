package UDP;




import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import converter.Object_And_Byte;
 
public class UDP_server extends Thread {
 
	private int Port = 0;
	private byte[] buffer = null;
	private DatagramPacket dp = null;
	private DatagramSocket ds = null;
	private DatagramPacket dps = null;
//	private String msg = null;
	private Object_And_Byte OAB = new Object_And_Byte();
	private Map<String, Object> msg = null;
	private boolean flag = true;

	
	public UDP_server(int _Port) throws IOException
	{
		this.Port = _Port;
		this.ds = new DatagramSocket(this.Port);
		this.buffer = new byte[65507];
		this.dp = new DatagramPacket(this.buffer, this.buffer.length);
		
		System.out.println("伺服器啟動於 : "
                + InetAddress.getLocalHost().getHostAddress() + ":" + this.ds.getLocalPort());

		System.out.println("**************************");
		
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("Action", "login");
		res.put("UserName", "A");
					
		byte[] byte_msg = this.OAB.Object_To_Byte(res);
		
    	this.dps = new DatagramPacket(byte_msg, byte_msg.length, InetAddress.getByName("163.18.49.23"), 8888);
    	this.ds.send(this.dps);
    	System.out.println("傳送登入訊息A");
    	
	}
	
	public void run() 
	{
	    while(this.flag)
	    {
	    	try 
	    	{	
	    		
				this.ds.receive(this.dp);
//				this.msg = new String(this.dp.getData(), 0, this.dp.getLength());
//		        System.out.println("傳來的訊息 : " + this.msg);
				this.msg = (Map<String, Object>) this.OAB.Byte_To_Object(this.dp.getData());
				
				if( ((String) this.msg.get("Action")).equals("HolePunching"))
				{
					
					System.out.println("收到了要求打洞訊息");
					System.out.println("來自 " + this.dp.getAddress() + ":" + this.dp.getPort());
					
					Map<String, Object> res = new HashMap<String, Object>();
					res.put("Action", "Hello");
								
					byte[] byte_msg = this.OAB.Object_To_Byte(res);
					
			    	this.dps = new DatagramPacket(byte_msg, byte_msg.length, InetAddress.getByName((String) this.msg.get("IP")), (int) this.msg.get("Port"));
			    	this.ds.send(this.dps);

					System.out.println("送出打洞訊息給 " + (String) this.msg.get("IP") + ":" + (int) this.msg.get("Port"));
					
				}
				
				else if( ((String) this.msg.get("Action")).equals("Hello"))
				{	
					
					System.out.println("收到了打洞訊息的 Hello");
					System.out.println("來自 " + this.dp.getAddress() + ":" + this.dp.getPort());
		
					System.out.println("**************************");
					
				}
				
			} 
	    	
	    	catch (IOException e) 
	    	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	catch (ClassNotFoundException e)
	    	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    }
	}
	
	public void close()
	{		
		this.flag = false;
	    this.ds.close();
	}
	
//	public void  on() throws Exception {
//        byte[] buffer = new byte[65507];
//        DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
//        DatagramSocket ds = new DatagramSocket(8888); // Set Server Port
//        System.out.println("伺服器啟動於 : "
//                + InetAddress.getLocalHost().getHostAddress() + ":" + ds.getLocalPort());
//        String msg = "No Message...";
//        while (true) {
//            ds.receive(dp);
//            msg = new String(dp.getData(), 0, dp.getLength());
//            System.out.println("傳來的訊息 : " + msg);
//        }
//    }
}