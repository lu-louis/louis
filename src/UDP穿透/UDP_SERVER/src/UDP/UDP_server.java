package UDP;




import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;



import converter.Object_And_Byte;
 
public class UDP_server extends Thread {
 
	private int Port = 0;
	private byte[] buffer = null;
	private DatagramPacket readPacket = null;
	private DatagramPacket sentPacket = null;
	private DatagramSocket ds = null;

	private Object_And_Byte OAB = new Object_And_Byte();
	private Map<String, Object> msg = null;
	private boolean flag = true;

	private String Host_A = null;
	private int Port_A = 0;
	
	private String Host_B = null;
	private int Port_B = 0;
	
	private int CNT = 0;
	
	public UDP_server(int _Port) throws IOException
	{
		this.Port = _Port;
		this.ds = new DatagramSocket(this.Port);
		this.buffer = new byte[65507];
		this.readPacket = new DatagramPacket(this.buffer, this.buffer.length);
		
		System.out.println("伺服器啟動於 : "
                + InetAddress.getLocalHost().getHostAddress() + ":" + this.ds.getLocalPort());

		System.out.println("**************************");
		   	
	}
	
	@SuppressWarnings("unchecked")
	public void run() 
	{
	    while(this.flag)
	    {
	    	try 
	    	{	
				this.ds.receive(this.readPacket);
				this.msg = (Map<String, Object>) this.OAB.Byte_To_Object(this.readPacket.getData());
				
				if( ((String) this.msg.get("Action")).equals("login"))
				{
					System.out.println("收到了登入訊息");
					System.out.println("來自 " + this.readPacket.getAddress() + ":" + this.readPacket.getPort());
					
					System.out.println("收到client: "+(String) msg.get("UserName"));
										
					if(((String) msg.get("UserName")).equals("A"))
					{
						this.Host_A = this.readPacket.getAddress().toString().replace("/", "");
						this.Port_A = this.readPacket.getPort();
						CNT++;
					}
					
					else if(((String) msg.get("UserName")).equals("B"))
					{
						this.Host_B = this.readPacket.getAddress().toString().replace("/", "");
						this.Port_B = this.readPacket.getPort();
						CNT++;
					}
				}
				
				if(CNT == 2)
				{
//					A=>B
					Map<String, Object> res = new HashMap<String, Object>();
					res.put("Action", "HolePunching");
					res.put("IP", this.Host_B);
					res.put("Port", this.Port_B);
					
					byte[] byte_msg = this.OAB.Object_To_Byte(res);
					
			    	this.sentPacket = new DatagramPacket(byte_msg, byte_msg.length, InetAddress.getByName(this.Host_A), this.Port_A);
			    	this.ds.send(this.sentPacket);
			    	
//					B=>A
					res = new HashMap<String, Object>();
					res.put("Action", "HolePunching");
					res.put("IP", this.Host_A);
					res.put("Port", this.Port_A);
					
					byte_msg = this.OAB.Object_To_Byte(res);
					
			    	this.sentPacket = new DatagramPacket(byte_msg, byte_msg.length, InetAddress.getByName(this.Host_B), this.Port_B);
			    	this.ds.send(this.sentPacket);
			    	
				}
				
			} 
	    	
	    	catch (IOException e) 
	    	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	catch (ClassNotFoundException e)
	    	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    }
	}
	
	public void sent(String host, int port, byte[] msg) throws IOException
	{
		DatagramPacket sentPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName(host), port);
    	this.ds.send(sentPacket);
	}
	
	public void close()
	{		
		this.flag = false;
	    this.ds.close();
	}
}