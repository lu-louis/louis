﻿var express = require('express');
var app = express();
var ExpressPeerServer = require('peer').ExpressPeerServer;
var https = require('https');
var fs = require('fs');

var ssl ={                                         //公私鑰和憑證書 位置
	key:fs.readFileSync('ssl/mykey.pem'),
	cert:fs.readFileSync('ssl/my-cert.pem')
}

var server =  https.createServer(ssl, app);

var options = {
    debug: true
}
	app.use('/Autocollection_hdfs', ExpressPeerServer(server, options));

	server.listen(8001);
	
	app.get(/(.*)\.(jpg|gif|png|ico|css|js|txt|webm)/i, function(req, res) {  
    res.sendfile(__dirname + "/" + req.params[0] + "." + req.params[1], function(err) {
        if (err) res.send(404);
    });
});

app.get('/', function(req, res) {
	res.sendfile(__dirname + '/Automatic_camera.html');
});

//socket
var net = require('net');

//node-datetime
var datetime = require('node-datetime');

//socket.io
var io = require('socket.io')(server);  

//mysql
var mysql = require('mysql');

//base64解碼器
function base64_decode(base64str, file) {
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var bitmap = new Buffer(base64str.replace(/^data:image\/(png|gif|jpeg);base64,/,''), 'base64');   //base64碼要刪除mime types再轉入否則圖片會損壞  
    // write buffer to file
    fs.writeFileSync(file, bitmap);
    console.log('******** File created from base64 encoded string ********');
}

var cnt=0;

	io.on('connection', function(socket) {
	  console.log('new connection');

	     socket.on('new', function(data)   //新的client登入
        { 
		   socket.name = data;       //設定新socket名稱為data
		   
        });
		
		socket.on('setproject', function(setproject)
        { 	
            var dir = './picture_project/'+setproject;		
			
			if (!fs.existsSync(dir)){
				fs.mkdirSync(dir);
				socket.emit('setproject_state',0); //無此資料夾直接創建
			}
			else{
				socket.emit('setproject_state',1); //資料夾已存在			
			}	
			
        });

		socket.on('project_DataURL', function(project_DataURL)
        { 		
			// var dt = datetime.create();
			// var fomratted = dt.format('Y-m-d_H-M-S');

			var dir = './picture_project/'+project_DataURL.project;

			if (!fs.existsSync(dir)){
				fs.mkdirSync(dir);
			}
			else{
				
                base64_decode(project_DataURL.DataURL,'picture_project/' + project_DataURL.project+ '/' + project_DataURL.project + "-" + project_DataURL.cnt +".jpeg");
				
			}
						   
        

            			
             // console.log(project_DataURL.DataURL);
						
		    
			
        });

		
		socket.on('ok', function(ok)
        { 	
		console.log("ok");
		cnt=0;
		
            //socket連線
			var client = net.connect(1010, 'localhost');
			
			client.write(ok);
			
			client.end();
			
        //socket_server
		// var textChunk = '';
		// var socket_server = net.createServer(function(tcp_socket) { //創建socket_server
			// tcp_socket.write('Echo server\r\n');
			// tcp_socket.on('data', function(data){
				
				// textChunk = data.toString('utf8');
				// console.log(textChunk);
				
				
				// socket.emit('result',textChunk); //回傳結果到html
			// });	
			
            // tcp_socket.on('end', () => {
			    // console.log('client disconnected');
				
			// socket_server.close(function () {   //關閉socket_server
			// console.log('server closed.');
			// socket_server.unref();
		    // });
			
			// });
			
		// });
					
		// socket_server.listen(8124, '163.18.49.23');
		
		
		});	
		





		
	socket.on('disconnect', function(){       //socket離線 


	});		  
	  
	});
	