package test_features;

public class feature_comparison {
	
	public int classification(int[] sd1_i,int[] sc1_i,int[] sc2_i,int[] sc3_i,int[] sd1_cut,int[] sc1_cut,int[] sc2_cut,int[] sc3_cut)
	{
		int total_score;
		int Difference;
		
		int sd1_score = Difference_score.avg_adder(sd1_i, sd1_cut);
		
		System.out.println("sd1_score: "+sd1_score);
		
		int sc1_score = Difference_score.hist_adder(sc1_i, sc1_cut);
		
		System.out.println("sc1_score: "+sc1_score);
		
		int sc2_score = Difference_score.hist_adder(sc2_i, sc2_cut);
		
		System.out.println("sc2_score: "+sc2_score);
		
		int sc3_score = Difference_score.hist_adder(sc3_i, sc3_cut);
		
		System.out.println("sc3_score: "+sc3_score);
		
		total_score = sd1_score + sc1_score + sc2_score + sc3_score;
		
		System.out.println("total_score: "+total_score);
		
		if(total_score>100000)
			Difference=1;
		else
			Difference=0;
		
		return Difference;
		
	}

}
