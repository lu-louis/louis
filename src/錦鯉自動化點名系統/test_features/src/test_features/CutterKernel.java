﻿package test_features;
//////////////////////////////////////////////////////
//  This code is write for Koi project to cut koi   //
//  Part     : Cutdown koi                          //
//  Version  : 1.6                                  //
//  detail   : 轉正拉直搭配用Cutter，有顏色去魚鰭的魚.      //
//  Engineer : Wei ,Eric                            //
//  Generate Date : 2016/10/31                      //
//  Release Date  : 2016/11/01                      //
//////////////////////////////////////////////////////

import java.io.File;
//import java.security.PublicKey;
import java.util.ArrayList;
//import java.util.Collections;
import java.util.List;
import java.util.Random;






//import javax.jws.Oneway;
import org.opencv.core.Core;
//import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
//import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class CutterKernel {
	static public String Cutter_Version = "version 1.6";
	public Mat source_image;
    int cnt = 0;
    public static String OUTPUT_PATH = "processed_image/";
    public static Scalar HSV_BLUE_MIN = new Scalar(92, 175, 100);
    public static Scalar HSV_BLUE_MAX = new Scalar(255, 256, 256);

    //==== constructor===============================
    public void GetImage() {
	this.source_image = new Mat();
    }//end of public koi_id() 

    public void GetImage(Mat fish_image) {
	this.source_image = fish_image.clone();
    }//end of koi_id(Mat fish_image)

    public void GetImage(String image_path) {
	//---check file is exists?-------------------------------------------------------------------
	File image_file = new File(image_path); //read image file
	if (image_file.exists()) {
	    System.out.println("read image file: " + image_file.getPath() + " success.");
	    this.source_image = Imgcodecs.imread(image_file.getPath());
	} else {
	    System.out.println("file: " + image_file.getPath() + "is not exist !");
	    System.exit(0); //program will normally end
	} //end of if(image_file.exists())
//TODO

	
	double tempC = (double) 1000 / source_image.cols();
	double tempR = (double) 1000 / source_image.rows();

	double scale = 0.0;
	
	
////////////////// cut  ///////////////////////////////////////////////////
	if ((source_image.cols() > 1000) || (source_image.rows() > 1000)) {
	    if (tempC >= tempR) {
	    	scale = tempR;
	    } else {
	    	scale = tempC;
	    }
	} else {

	    if (tempC >= tempR) {
	    	scale = tempR;
	    } else {
	    	scale = tempC;
	    }
	}
    Imgproc.resize(this.source_image, this.source_image,
	    new Size(source_image.cols() * scale, source_image.rows() * scale), 0.0, 0.0, Imgproc.INTER_LINEAR);

    }//end of koi_id(Mat fish_image)

    //=== functions =================================
    
    public Mat putTextAndExport(Mat image, String fileName, String text) {
	Mat text_image = image.clone();
	Imgproc.putText(text_image, text, new Point(20, 40), Core.FONT_HERSHEY_COMPLEX, 1.0, new Scalar(111, 222, 333));
	this.export(text_image, fileName);
	return text_image;
    }//public void putTextAndRelease
    
    public void export(Mat image, String fileName){
		try {
		    Imgcodecs.imwrite(CutterKernel.OUTPUT_PATH + fileName + ".bmp", image);
		} catch (Exception e) {
		    System.out.println("file write error!");
		    System.out.println(e.getMessage());
		}
    }//end of void Export(String fileName)
    
    public int gary_value = 0; 
    
    public void setSourceImage(Mat newSource) {
	this.source_image = newSource;
    }//end of void setSourceImage(Mat newSource)
    
    
    
  //TODO
    
    public Mat detectColor(Mat srcImage, Scalar minValue, Scalar maxValue,int stream) {
	Mat tempImg = srcImage.clone();
	Mat theColor = new Mat();
	Mat yCbCrImage = new Mat();
	
	List<Mat> yCbCrChannels = new ArrayList<Mat>();
	Imgproc.cvtColor(tempImg, yCbCrImage, Imgproc.COLOR_BGR2YCrCb);//2HLS 3HSV 4LUV 5YUV 6YCBCR
	Core.split(yCbCrImage, yCbCrChannels);
	
	
	Mat gray = new Mat();
//	Core.addWeighted(yCbCrChannels.get(1), 0.5, yCbCrChannels.get(2), 0.5, 0, gray);
	
//	Core.normalize(yCbCrChannels.get(0), yCbCrChannels.get(0),0, 255,Core.NORM_MINMAX, -1);
//	Core.normalize(yCbCrChannels.get(2), yCbCrChannels.get(2),0, 255,Core.NORM_MINMAX, -1);
	this.export(yCbCrChannels.get(0), "grayCrBin0");
	this.export(yCbCrChannels.get(1), "grayCrBin1");
	this.export(yCbCrChannels.get(2), "grayCrBin2");
	
	//////////////////////////
//	int rows = source_image.rows();
//	int cols = source_image.cols();
//	int sum = 0;
//	for( int i = 0; i < rows; i++){ 
//	    for( int j = 0; j < cols; j++ ){
//	    	sum = sum + (int)(yCbCrChannels.get(0).get(i, j))[0];
//	    }
//	}
//	int mean = sum/(cols*rows);
	
	
	
	gray= yCbCrChannels.get(0).clone();
//	this.export(gray, "gray0" + stream);	
	
	Mat calcHis = new Mat();
	List<Mat> matList = new ArrayList<Mat>();
	
//	Imgproc.equalizeHist(yCbCrChannels.get(2), yCbCrChannels.get(2));
	
//	this.export(yCbCrChannels.get(2), "CrBin0_" + stream);			
																		
	matList.add(yCbCrChannels.get(2));
//	Core.normalize(yCbCrChannels.get(2), yCbCrChannels.get(2),0, 255,Core.NORM_MINMAX, -1);

	this.export(yCbCrChannels.get(2), "grayCrBin2_norm");
	MatOfFloat ranges=new MatOfFloat(0,255);
	MatOfInt hstSize = new MatOfInt(255);
	Imgproc.calcHist(	matList, 
    	                new MatOfInt(0), 
    	                new Mat(), 	
    	                calcHis , 
    	                hstSize , 
    	                ranges);

	Core.normalize(calcHis, calcHis,0, 255,Core.NORM_MINMAX, -1);
	export(calcHis, "calcHis"+stream);

	
	/////////////////////////////////// get image_th ///////////////////
	float image_th = 0;
//	float image_th_next = 0;
	float image_big_temp = 0;
	float[] data = {0};
	int image_big1 = 0;
	int image_big2 = 0;
//	int wave_cnt = 0;
	for(int i = 0 ; i < 255 ; i ++){
		calcHis.get(i, 0, data);
		image_th = data[0];
		if(image_th > image_big_temp){image_big_temp = image_th ;image_big1 = i;}
	}
	calcHis.get(image_big1, 0, data);
	image_th = data[0];
//	for(int i = image_big1 ; i < 255 ; i ++){
//		calcHis.get(i, 0, data);
//		image_th = data[0];
//		calcHis.get(i+1, 0, data);
//		image_th_next = data[0];
//		if((int)image_th - (int)image_th_next > 0){
//			calcHis.get(i, 0, data);
//			image_th = data[0];
//			data[0] = 0;
//			wave_cnt = 0;
//			calcHis.put(i, 0, data);
//		}
//		else{
//			data[0] = 0;
//			calcHis.put(i, 0, data);
//			if(wave_cnt >= 5){
//				break;
//			}
//			else{
//				wave_cnt++;
//			}
//		}
//	}
//	wave_cnt = 0;
//	for(int i = (image_big1-20) ; i > 0 ; i --){
//		calcHis.get(i, 0, data);
//		image_th = data[0];
//		calcHis.get(i+1, 0, data);
//		image_th_next = data[0];
//		if((int)image_th - (int)image_th_next > 0){
//			calcHis.get(i, 0, data);
//			image_th = data[0];
//			data[0] = 0;
//			wave_cnt = 0;
//			calcHis.put(i, 0, data);
//		}
//		else{
//			data[0] = 0;
//			calcHis.put(i, 0, data);
//			if(wave_cnt >= 3){
//				break;
//			}
//			else{
//				wave_cnt++;
//			}
//		}
//	}
	image_big_temp = 0;
	image_th=0;
	image_big2 = image_big1;
	System.out.println("image_big1 = " + image_big1);
	int step_image_big1 = 0;//27-(image_big1/10);
	while(image_big1-image_big2 < 15 && image_big2 != 0){
		image_big1 = image_big2;
		for(int i = 0 ; i < (image_big1-step_image_big1) ; i ++){
			calcHis.get(i, 0, data);
			image_th = data[0];
			if(image_th >= image_big_temp){image_big_temp = image_th ;image_big2 = i;}
		}
		image_th=0;
		image_big_temp = 0;
		System.out.println("image_big2 = " + image_big2);
	}
//	export(calcHis, "calcHis_big_"+stream);
	
	
	System.out.println("image_big2 = " + image_big2);
	
//	if(mean >= 100 && mean <= 140){
//		image_th = 145 + (mean-100)/4;
//	}
//	else if(mean < 100){
//		image_th = 145;
//	}
//	else if(mean > 140){
		
//	}
	Mat theColor2 = new Mat(gray.size(),CvType.CV_16UC1);
	Mat theColor3 = new Mat(gray.size(),CvType.CV_16UC1);
//	image_big2 = image_big1 - 7;
//	image_big1 = image_big1;
//	if(image_big1 > 20) image_big2 = image_big1-2;else image_big2 = 0;
//	if(image_big2 > 40) image_big2 = image_big1 - 40;else image_big2 = 0;
	double image_step = 1.0/Math.abs(image_big1-image_big2);
	double image_ab = 0.0;
//	double image_big_mid = ((image_big2 + image_big1)/2)-image_big2;
//	System.out.println("image_step = " + image_step);
//	List<Mat> theColor_list = new ArrayList<Mat>();
	
	for(int i = 0 ; i < image_big1 - image_big2 ; i++){
		Imgproc.threshold(yCbCrChannels.get(2), theColor, (i+image_big2) , 255, Imgproc.THRESH_BINARY_INV);//155 >> 140+-
//		Imgproc.threshold(yCbCrChannels.get(2), theColor2, image_big2, 255, Imgproc.THRESH_BINARY_INV);//155 >> 140+-
		if(i == 0)theColor3 = Mat.zeros(theColor.size(),theColor.type());
		
		
//		if(i <= image_big_mid) image_ab = ((i/image_big_mid)*image_step)+(image_step/2);else image_ab = (((image_big_mid*2 - i)/image_big_mid)*image_step)+(image_step/2);
		
		image_ab = image_step;
//		System.out.println("image_ab = " + image_ab);
//		Core.addWeighted(C_mat, 0.5, C_mat_2, 0.5, 0, C_mat_3);
		Core.addWeighted(theColor3, (1.0), theColor, image_ab, 0, theColor2);
		theColor3 = theColor2.clone();
	}
	
//	Core.addWeighted(theColor, 1, C_mat_7, 1, 0, gray);
	export(theColor2, "theColor_"+stream);
	Imgproc.threshold(theColor2, theColor2, 100 , 255, Imgproc.THRESH_BINARY);//155 >> 140+-
	
//	Imgproc.dilate(theColor2, theColor2, erodeElement);
//	Imgproc.erode(theColor2, theColor2, erodeElement);
//	this.export(theColor2, "C_mat_7___");
//	erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
//	Imgproc.erode(theColor2, theColor2, erodeElement);
//	Imgproc.dilate(theColor2, theColor2, erodeElement);

//	export(C_mat_7, "C_mat_7_"+stream);
//	gray = Mat.zeros(theColor2.size(), theColor2.type());
	
//	C_mat_7.copyTo(gray, theColor2);
	Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
	export(gray, "theColor2_"+stream);
	
//	for(int i = 0 ; i < 15 ; i++){
//		erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
//		Imgproc.erode(theColor2, theColor2, erodeElement);
//	}
//	for(int i = 0 ; i < 15 ; i++){
//		erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
//		Imgproc.dilate(theColor2, theColor2, erodeElement);
//	}
	export(theColor2, "gray_erooo"+stream);
	
	//////////////////////////////////// THRESH_OTSU for WK , THRESH_BINARY for others  ///////////////
	
	
	
	Mat C_mat = new Mat(gray.size(),CvType.CV_16UC1);
	Mat C_mat_2 = new Mat(gray.size(),CvType.CV_16UC1);
	Mat C_mat_3 = new Mat(gray.size(),CvType.CV_16UC1);
	Mat C_mat_4 = new Mat(gray.size(),CvType.CV_16UC1);
	Mat C_mat_5 = new Mat(gray.size(),CvType.CV_16UC1);
	Mat C_mat_6 = new Mat(gray.size(),CvType.CV_16UC1);
	Mat C_mat_7 = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_mat_9 = new Mat(gray.size(),CvType.CV_16UC1);

	List<Mat> C_mat_8 = new ArrayList<Mat>();
	
//	for(int i = 0 ; i < 1 ; i ++){
		gray = theColor2.clone();

		this.export(gray, "gray_");
		
		Imgproc.Sobel(gray, C_mat, CvType.CV_8UC1, 1, 0, 3, 1, 0);
		Imgproc.Sobel(gray, C_mat_2, CvType.CV_8UC1, 0, 1, 3, 1, 0);
		Imgproc.Sobel(gray, C_mat_4, CvType.CV_8UC1, 1, 0, 3, -1, 0);
		Imgproc.Sobel(gray, C_mat_5, CvType.CV_8UC1, 0, 1, 3, -1, 0);
		Core.addWeighted(C_mat, 0.5, C_mat_2, 0.5, 0, C_mat_3);
		Core.addWeighted(C_mat_4, 0.5, C_mat_5, 0.5, 0, C_mat_6);
		Core.addWeighted(C_mat_3, 0.5, C_mat_6, 0.5, 0, C_mat_7);
		
		Core.normalize(C_mat_7, C_mat_7,0, 255,Core.NORM_MINMAX, -1);
		C_mat_8.add(C_mat_7.clone());
//		this.export(C_mat_7, "C_mat_7__"+i + "__" + stream);
		
//	}
	
	this.export(C_mat_8.get(0), "C_mat_8.get(0)");
//	this.export(C_mat_8.get(1), "C_mat_8.get(1)" + stream);
//	this.export(C_mat_8.get(2), "C_mat_8.get(2)" + stream);
//	Core.addWeighted(C_mat_8.get(0), 1, C_mat_8.get(1), 1, 0, C_mat_9);
//	Core.addWeighted(C_mat_9, 0.66, C_mat_8.get(2), 0.33, 0, C_mat_9);
	
//	this.export(C_mat_9, "C_mat_7___" + stream);	
//	gray = yCbCrChannels.get(0).clone();
	
//	this.export(gray, "gray1" + stream);
	
	
//	Mat C_1_mat = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_1_mat_2 = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_1_mat_3 = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_1_mat_4 = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_1_mat_5 = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_1_mat_6 = new Mat(gray.size(),CvType.CV_16UC1);
//	Mat C_1_mat_7 = new Mat(gray.size(),CvType.CV_16UC1);
//	
//	Imgproc.Sobel(gray, C_1_mat, CvType.CV_8UC1, 1, 0, 3, 1, 0);
//	Imgproc.Sobel(gray, C_1_mat_2, CvType.CV_8UC1, 0, 1, 3, 1, 0);
//	Imgproc.Sobel(gray, C_1_mat_4, CvType.CV_8UC1, 1, 0, 3, -1, 0);
//	Imgproc.Sobel(gray, C_1_mat_5, CvType.CV_8UC1, 0, 1, 3, -1, 0);
//	Core.addWeighted(C_1_mat, 0.5, C_1_mat_2, 0.5, 0, C_1_mat_3);
//	Core.addWeighted(C_1_mat_4, 0.5, C_1_mat_5, 0.5, 0, C_1_mat_6);
//	Core.addWeighted(C_1_mat_3, 0.5, C_1_mat_6, 0.5, 0, C_1_mat_7);
//	
//	Core.normalize(C_1_mat_7, C_1_mat_7,0, 255,Core.NORM_MINMAX, -1);
//	Imgproc.equalizeHist(C_mat_7, C_mat_7);
//	
//	Core.addWeighted(C_1_mat_7, 1, C_mat_7, 1, 0, C_1_mat_7);
//	this.export(C_1_mat_7, "C_mat_7_1_" + stream);	
//	
	Imgproc.threshold(C_mat_7, C_mat_7, 25, 255, Imgproc.THRESH_BINARY);
	
	erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
	Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
	Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
	this.export(C_mat_7, "C_mat_7___");	
	erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
	Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
	Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
	
//	for(int i = 2 ; i < 10 ; i ++){
	erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
	Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
	Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
		
//	}
	
	export(C_mat_7, "C_mat_7_erodil");
	
	ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
	List<ArrayList<MatOfPoint>> array_contours = new ArrayList<ArrayList<MatOfPoint>>();
	Mat drawCon = new Mat();
	Mat dest = new Mat();
		Core.inRange(C_mat_7, new Scalar(20), new Scalar(255), dest);
		Imgproc.findContours(dest,
		contours,
		drawCon, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		array_contours.add(contours) ;
	for(int i = 0; i < array_contours.size() ; i++){
		for (int k = 0; k < array_contours.get(i).size(); k++) {
			Imgproc.drawContours(dest, array_contours.get(i), k, new Scalar(0, 255, 255), 1);
		}
	}
	
	
	Core.normalize(C_mat_7, C_mat_7,0, 255,Core.NORM_MINMAX, -1);
	export(C_mat_7, "C_mat_7");
	erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
//	Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
//	Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
//	Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
//	Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
	
//	export(C_mat_7, "C_mat_7_erodil");
	
	
	
	Core.addWeighted(theColor2, 1, C_mat_7, 1, 0, gray);

	export(theColor2, "theColor2theColor2_"+stream);
	//contour 全部進去floodFill
	
//	for()
//	Imgproc.floodFill(theColor, new Mat(), new Point(C_mat_7.width()/2,C_mat_7.height()/2), new Scalar(255));
//	
	
//	Imgproc.dilate(theColor, theColor, erodeElement);
//	Imgproc.dilate(theColor, theColor, erodeElement);
//	Imgproc.erode(theColor, theColor, erodeElement);
//	Core.addWeighted(theColor, 0.5, theColor2, 0.5, 0, theColor3);
	
//	Core.addWeighted(theColor, 0.5, C_mat_7, 0.5, 0, theColor);
//	export(theColor3, "floodFill"+stream);

//	erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(8, 8));
	
//	Imgproc.erode(theColor, theColor, erodeElement);
//	Imgproc.erode(theColor, theColor, erodeElement);

	//dilate with larger element so make sure object is nicely visible
//	Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(8, 8));
//	Imgproc.dilate(theColor, theColor, dilateElement);

//	this.export(theColor, "detect color_"+stream);
	return theColor2;
    }//end of Mat detectColor(Scalar minValue, Scalar maxValue)
    
    
    
    
    

    public Mat removeColor(Mat srcImage, Mat matMask) {

	Mat theColor = new Mat();
	srcImage.copyTo(theColor, matMask);
//	Core.absdiff(source_image, theColor, withoutColor); //source image - only blue image
	return theColor;
	
    }//end of Mat removeColor(Scalar minValue, Scalar maxValue)

    
    public Mat fillBackgroud(Mat srcImage, Scalar color) {
	//== draw contours ========================================================================================
	List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
	Mat hierarchy = new Mat();
	Mat tempImage = srcImage.clone();

	//=== binarization =======================================================================================
	Imgproc.cvtColor(srcImage, tempImage, Imgproc.COLOR_BGR2GRAY);
	Imgproc.threshold(tempImage, tempImage, 10, 255, Imgproc.THRESH_BINARY);
	
	//=== find the largest contour ===========================================================================
	Imgproc.findContours(tempImage, contour, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_TC89_KCOS);

	int largestIdx = 0;
	double tempArea = Imgproc.contourArea(contour.get(0));
	double largestArea = tempArea;

	for (int i = 1; i < contour.size(); i++) {
	    tempArea = Imgproc.contourArea(contour.get(i));
	    //=== recover the largest value==============
	    if (tempArea > largestArea) {
		largestIdx = i;
		largestArea = tempArea;
	    }
	} //end of for (int i = 1; i < contour.size(); i++)

	MatOfPoint tempContours = contour.get(largestIdx);
	contour.clear();
	contour.add(tempContours);

	//=== Draw contours and make the mask ===================================================================
	Mat drawing = Mat.zeros(tempImage.size(), tempImage.type()); //create a all zeros image
	for (int i = 0; i < contour.size(); i++) {
	    //drawContours(drawing, contour, i, color, 2, 8, hierarchy, 0, new Point());
	    Imgproc.drawContours(drawing, contour, i, color);
	    Imgproc.fillPoly(drawing, contour, new Scalar(255, 255, 255));
	} //end of for (int i = 0; i < contour.size(); i++)

	Mat tempList = new Mat();
	srcImage.copyTo(tempList, drawing);

	Mat mask = new Mat();
	Imgproc.threshold(drawing, mask, 10, 255, Imgproc.THRESH_BINARY_INV);

	Mat colorImage = new Mat(srcImage.size(), srcImage.type());
	colorImage.setTo(color, mask); //fill the color without the contour

	tempImage = srcImage.clone();
	tempImage.setTo(color, mask); //fill the color to source image with a mask

	return tempImage;
    }//end of public void fillBackgroud(List<Mat> sourceList, Scalar color)


//TODO
    public List<Mat> rotateFishHead2Bottom(Mat sigleFishImage, Point org_point) {
    
	//=== Find hull =========================================================================================
	List<Point> hull = new ArrayList<Point>();
	MatOfInt hullInt = new MatOfInt();
	Point hullPoint = new Point();
	//=== #################################################################################################
	Mat temp = sigleFishImage.clone();
	Mat GrayImage = new Mat();
	Imgproc.cvtColor(temp, GrayImage, Imgproc.COLOR_BGR2GRAY);

	//---find max---------
	List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
	Mat hierarchy = new Mat();

	int contourIndex = this.getLargestCotour(GrayImage, contour, hierarchy);

	//=== Draw contours and hull===========================================================================
	Mat drawing = Mat.zeros(temp.size(), CvType.CV_8UC3); //create a all zeros image
	
	Imgproc.convexHull(contour.get(contourIndex), hullInt);
	
	
	//--- translate the MatOfInt to Point List--------------------------------------
	for (int i = 0; i < hullInt.toList().size(); i++) {
	    hullPoint = contour.get(contourIndex).toList().get(hullInt.toList().get(i));
	    hull.add(hullPoint);
	}
	
	//最大contour畫線
	for (int i = 0; i < contour.size(); i++) {
	    Imgproc.drawContours(drawing, contour, i, new Scalar(255, 255, 255));
	} //end of for (int i = 0; i < contour.size(); i++)
	
	//hull 畫線
	for (int i = 0; i < hull.size(); i++) {
	    if (i == (hull.size() - 1)) {
		Imgproc.line(drawing, hull.get(i), hull.get(0), new Scalar(255, 255, 255));
	    } else {
		Imgproc.line(drawing, hull.get(i), hull.get(i + 1), new Scalar(255, 255, 255));
	    }
	} //end of for (int i = 0; i < hull.size(); i++)

	//=== moment ===========================================================================================
	double scale_mot = 1.0;
	if ((GrayImage.cols() > 500) || (GrayImage.rows() > 500)) {
	    if (GrayImage.cols() > GrayImage.rows()) {
	    	scale_mot = 500.0/GrayImage.cols();
	    } else {
	    	scale_mot = 500.0/GrayImage.rows();
	    }
	} else {
		scale_mot =  1.0;
	}
	Imgproc.resize(GrayImage, GrayImage,
		    new Size(GrayImage.cols() * scale_mot, GrayImage.rows() * scale_mot), 0.0, 0.0, Imgproc.INTER_CUBIC);
	Moments mot = Imgproc.moments(GrayImage);
	Point center = new Point(0, 0);
	center.x = (mot.m10 / mot.m00)/scale_mot;
	center.y = (mot.m01 / mot.m00)/scale_mot;

	Imgproc.circle(drawing, new Point(center.x, center.y), 5, new Scalar(0, 0, 255));
//	for (int i = 0; i < hull.size(); i++) {
//	Imgproc.circle(drawing, hull.get(i), 5, new Scalar(0, 0, 255));}
	
	//=== find longest distance====================================================================================
	int maxIdx = 0;
	int secIdx = 0;
	double dist = 0.0;
	double max_dist = 0.0;

	for (int i = 0; i < hull.size(); i++) {
	    for (int j = 0; j < hull.size(); j++) {
	    	dist = Math.hypot(hull.get(i).x - hull.get(j).x, hull.get(i).y - hull.get(j).y);
			if (dist > max_dist) {
			    maxIdx = i;
			    secIdx = j;
			    max_dist = dist;
			}
	    }
	} //end of for (int i = 0; i < hull.size(); i++)

	if (Math.hypot(hull.get(maxIdx).x - center.x, hull.get(maxIdx).y - center.y) < 
		Math.hypot(hull.get(secIdx).x - center.x, hull.get(secIdx).y - center.y)) {
		int temp_Idx = 0;
		temp_Idx = secIdx;
	    secIdx = maxIdx;
	    maxIdx = temp_Idx;
	}
	Imgproc.line(drawing, hull.get(maxIdx), hull.get(secIdx), new Scalar(255, 255, 255));

	int minIdx = 0;
	double min_dist = max_dist;

	for (int i = 0; i < contour.get(contourIndex).toList().size(); i++) {
	    dist = Math.hypot(center.x - contour.get(contourIndex).toList().get(i).x,
		    center.y - contour.get(contourIndex).toList().get(i).y);
	    if (dist < min_dist) {
		minIdx = i;
		min_dist = dist;
	    }
	}

	Imgproc.circle(drawing, contour.get(contourIndex).toList().get(minIdx), 3, new Scalar(255, 0, 255));
//	System.out.println("=========================="+cnt+"=========================================");
//TODO
	
//	this.export(drawing, "test");
	cnt++;
	//=== find the head's angle=============================================================================
	//--create a zeros matrix with size 2*center to farest point -------------------------------------------
	if (sigleFishImage.width() > sigleFishImage.height()) {
	    dist = sigleFishImage.width();
	} else {
	    dist = sigleFishImage.height();
	}
	//可以改成斜邊當size防止畫面被切掉(現在直接用最大邊*2)
	Mat tempRotate = Mat.zeros(new Size(dist*2, dist*2), sigleFishImage.type());

	Point startPt = new Point(Math.round(tempRotate.width() / 2 - center.x),
							  Math.round(tempRotate.height() / 2 - center.y));
	Point midPt = new Point(Math.round(tempRotate.width() / 2), Math.round(tempRotate.height() / 2));
	
	Point midPt_org_point = new Point(source_image.width()/2 ,source_image.height()/2);
	
	
	Rect tempRect = new Rect(startPt, new Size(sigleFishImage.width(), sigleFishImage.height()));
	Mat tempROI = tempRotate.submat(tempRect);
	sigleFishImage.copyTo(tempROI);
	tempRotate.submat(tempRect);


	Point headPt = new Point(startPt.x + hull.get(secIdx).x, startPt.y + hull.get(secIdx).y);
	Point tailPt = new Point(startPt.x + hull.get(maxIdx).x, startPt.y + hull.get(maxIdx).y);

	
	double headDist = Math.hypot(headPt.x - midPt.x, headPt.y - midPt.y);
	Point refPoint = new Point(tailPt.x + Math.round(headDist), tailPt.y);
	double theta = this.getAngle(tailPt, headPt, refPoint);
	//=== rotate head to bottom ==========================================================================
	double angle = 0;
	if (headPt.y >= tailPt.y) {
	    angle = 270 + theta;
	} else {
	    angle = 270 - theta;
	}

	
	
	if (source_image.width() > source_image.height()) {
	    dist = source_image.width();
	} else {
	    dist = source_image.height();
	}
	Mat org_imgRotate = Mat.zeros(new Size(dist*2, dist*2), this.source_image.type());
	
	Rect org_tempRect = new Rect(midPt_org_point, new Size(source_image.width(), source_image.height()));
	
	Mat org_tempROI = org_imgRotate.submat(org_tempRect);
	source_image.copyTo(org_tempROI);
	org_imgRotate.submat(org_tempRect);
	
	
//	Imgproc.circle(org_imgRotate, startPt, 3, new Scalar(0, 255, 255));
//	Imgproc.circle(tempRotate, midPt, 3, new Scalar(0, 255, 255));
////	Imgproc.circle(org_imgRotate, startPt, 3, new Scalar(0, 255, 255));
//	
//	Imgproc.circle(tempRotate, headPt, 3, new Scalar(0, 255, 0));
//	Imgproc.circle(tempRotate, refPoint, 3, new Scalar(0, 127, 255));
//	this.export(tempRotate, "point_" + stream_no);
	
	Point org_point_center = new Point(org_point.x + source_image.width()/2 + sigleFishImage.width()/2,
									   org_point.y + source_image.height()/2 + sigleFishImage.height()/2);
	Mat rotMat = Imgproc.getRotationMatrix2D(midPt, angle, 1.0);
	Mat rotMat_org = Imgproc.getRotationMatrix2D(org_point_center, angle, 1.0);
	
	Imgproc.warpAffine(tempRotate, tempRotate, rotMat, tempRotate.size());
	Imgproc.warpAffine(org_imgRotate, org_imgRotate, rotMat_org, org_imgRotate.size());
//	export(org_imgRotate, "org_img_afterRotate_"+cnt);
	
	//=== resize =========================================================================================
	contour.clear();
	Imgproc.cvtColor(tempRotate, GrayImage, Imgproc.COLOR_BGR2GRAY);
	//---find max---------
	contour = new ArrayList<MatOfPoint>();
	contourIndex = this.getLargestCotour(GrayImage, contour, hierarchy);
	Rect bounding = Imgproc.boundingRect(contour.get(contourIndex));
	Mat ROI = tempRotate.submat(bounding);
//	export(ROI, "ROI");
	List<Mat> return_ROI = new ArrayList<Mat>();
	
	Mat org_ROI = new Mat(ROI.size(),ROI.type());
	int begin_x = 0;
	int begin_y = 0;
	int end_x = 0;
	int end_y = 0;
	if((int)(org_point_center.y-(ROI.height()*3/4)) < 0){begin_y = 0;}else{ begin_y = (int)(org_point_center.y-(ROI.height()*3/4));}
	if((int)(org_point_center.x-(ROI.width()*3/4))  < 0){begin_x = 0;}else{ begin_x = (int)(org_point_center.x-(ROI.width()*3/4));}
	if((int)(org_point_center.y+(ROI.height()*3/4)) > (int)source_image.height()*2){end_y = (int)(source_image.height())*2;}else{ end_y = (int)(org_point_center.y+(ROI.height()*3/4));}
	if((int)(org_point_center.x+(ROI.width()*3/4))  > (int)source_image.width()*2) {end_x = (int)(source_image.width())*2; }else{ end_x = (int)(org_point_center.x+(ROI.width()*3/4));}
	org_ROI = org_imgRotate.submat(begin_y,end_y,begin_x,end_x);
//	export(org_ROI, "ROI");
	return_ROI.add(ROI);
	return_ROI.add(org_ROI);
	return return_ROI;
    }//end of rotateFishHead2Bottom()

	double getAngle(Point pSrc, Point p1, Point p2) {
	double angle = 0.0f;

	double va_x = p1.x - pSrc.x;
	double va_y = p1.y - pSrc.y;

	double vb_x = p2.x - pSrc.x;
	double vb_y = p2.y - pSrc.y;

	double productValue = (va_x * vb_x) + (va_y * vb_y);
	double va_val = Math.sqrt(va_x * va_x + va_y * va_y);
	double vb_val = Math.sqrt(vb_x * vb_x + vb_y * vb_y);
	double cosValue = productValue / (va_val * vb_val);

	if (cosValue < -1 && cosValue > -2)
	    cosValue = -1;
	else if (cosValue > 1 && cosValue < 2)
	    cosValue = 1;

	angle = Math.acos(cosValue) * 180 / Math.PI;

	return angle;
    }//end of double getAngle(Point pSrc, Point p1, Point p2)

	public Mat RemoveEdgeCut(Mat src) {
		List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
    	Imgproc.findContours(src.clone(), contour, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
    	//=== Draw contours======================================================================================
    	Mat drawing = Mat.zeros(src.size(), src.type()); //create a all zeros image
//    	Random rng = new Random();
    	for (int i = 0; i < contour.size(); i++) {
//    	    Scalar color = new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256));
//    	    Imgproc.drawContours(drawing, contour, i, color, 2, 8, hierarchy, 2, new Point(0, 0));
    	    Rect bounding = Imgproc.boundingRect(contour.get(i));
			if(bounding.width < src.width()/10 && bounding.height < src.height()/10 ){
				contour.remove(i);
				i--;
			}
			else if(bounding.x <= 1 || bounding.x+bounding.width >= src.width()-1 ||
					bounding.y <= 1 || bounding.y+bounding.height >= src.height()-1){
				contour.remove(i);
				i--;
			}
    	} //end of for (int i = 0; i < contour.size(); i++)
    	System.out.println("contour = " + contour.size());
    	for (int i = 0; i < contour.size(); i++) {
    		Imgproc.drawContours(drawing, contour, i, new Scalar(255), Core.FILLED);
    	}
    	this.export(drawing, "drawing");
    	return drawing;
	}
    
	public Mat DrawCon(Mat src) {
		Mat tempImg = src.clone();
//		Mat theColor = new Mat();
		Mat yCbCrImage = new Mat();
		
		List<Mat> yCbCrChannels = new ArrayList<Mat>();
		Imgproc.cvtColor(tempImg, yCbCrImage, Imgproc.COLOR_BGR2YCrCb);//2HLS 3HSV 4LUV 5YUV 6YCBCR
		Core.split(yCbCrImage, yCbCrChannels);
		Mat gray = yCbCrChannels.get(0).clone();
//		Core.normalize(gray, gray,0, 255,Core.NORM_MINMAX, -1);
		Mat C_mat = new Mat(gray.size(),CvType.CV_16UC1);
		Mat C_mat_2 = new Mat(gray.size(),CvType.CV_16UC1);
		Mat C_mat_3 = new Mat(gray.size(),CvType.CV_16UC1);
		Mat C_mat_4 = new Mat(gray.size(),CvType.CV_16UC1);
		Mat C_mat_5 = new Mat(gray.size(),CvType.CV_16UC1);
		Mat C_mat_6 = new Mat(gray.size(),CvType.CV_16UC1);
		Mat C_mat_7 = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_mat_9 = new Mat(gray.size(),CvType.CV_16UC1);
//
//		List<Mat> C_mat_8 = new ArrayList<Mat>();
//		
//		for(int i = 0 ; i < 1 ; i ++){
			

			this.export(gray, "gray_");
			
			Imgproc.Sobel(gray, C_mat, CvType.CV_8UC1, 1, 0, 3, 1, 0);
			Imgproc.Sobel(gray, C_mat_2, CvType.CV_8UC1, 0, 1, 3, 1, 0);
			Imgproc.Sobel(gray, C_mat_4, CvType.CV_8UC1, 1, 0, 3, -1, 0);
			Imgproc.Sobel(gray, C_mat_5, CvType.CV_8UC1, 0, 1, 3, -1, 0);
			Core.addWeighted(C_mat, 1, C_mat_2, 1, 0, C_mat_3);
			Core.addWeighted(C_mat_4, 1, C_mat_5, 1, 0, C_mat_6);
			Core.addWeighted(C_mat_3, 1, C_mat_6, 1, 0, C_mat_7);
			Core.normalize(C_mat_7, C_mat_7,0, 255,Core.NORM_MINMAX, -1);
//			C_mat_8.add(C_mat_7.clone());
			this.export(C_mat_7, "C_mat_7__C");
			
//		}
		
//		this.export(C_mat_8.get(0), "C_mat_8.get(0)");
//		this.export(C_mat_8.get(1), "C_mat_8.get(1)" + stream);
//		this.export(C_mat_8.get(2), "C_mat_8.get(2)" + stream);
//		Core.addWeighted(C_mat_8.get(0), 1, C_mat_8.get(1), 1, 0, C_mat_9);
//		Core.addWeighted(C_mat_9, 0.66, C_mat_8.get(2), 0.33, 0, C_mat_9);
		
//		this.export(C_mat_9, "C_mat_7___" + stream);	
//		gray = yCbCrChannels.get(0).clone();
		
//		this.export(gray, "gray1" + stream);
		
		
//		Mat C_1_mat = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_1_mat_2 = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_1_mat_3 = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_1_mat_4 = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_1_mat_5 = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_1_mat_6 = new Mat(gray.size(),CvType.CV_16UC1);
//		Mat C_1_mat_7 = new Mat(gray.size(),CvType.CV_16UC1);
	//	
//		Imgproc.Sobel(gray, C_1_mat, CvType.CV_8UC1, 1, 0, 3, 1, 0);
//		Imgproc.Sobel(gray, C_1_mat_2, CvType.CV_8UC1, 0, 1, 3, 1, 0);
//		Imgproc.Sobel(gray, C_1_mat_4, CvType.CV_8UC1, 1, 0, 3, -1, 0);
//		Imgproc.Sobel(gray, C_1_mat_5, CvType.CV_8UC1, 0, 1, 3, -1, 0);
//		Core.addWeighted(C_1_mat, 0.5, C_1_mat_2, 0.5, 0, C_1_mat_3);
//		Core.addWeighted(C_1_mat_4, 0.5, C_1_mat_5, 0.5, 0, C_1_mat_6);
//		Core.addWeighted(C_1_mat_3, 0.5, C_1_mat_6, 0.5, 0, C_1_mat_7);
	//	
//		Core.normalize(C_1_mat_7, C_1_mat_7,0, 255,Core.NORM_MINMAX, -1);
//		Imgproc.equalizeHist(C_mat_7, C_mat_7);
	//	
//		Core.addWeighted(C_1_mat_7, 1, C_mat_7, 1, 0, C_1_mat_7);
//		this.export(C_1_mat_7, "C_mat_7_1_" + stream);
	//	
		Imgproc.threshold(C_mat_7, C_mat_7, 25, 255, Imgproc.THRESH_BINARY);
		
		Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
		Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
		Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
		this.export(C_mat_7, "C_mat_7___");	
		erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
		Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
		Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
		
//		for(int i = 2 ; i < 10 ; i ++){
		erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
		Imgproc.dilate(C_mat_7, C_mat_7, erodeElement);
		Imgproc.erode(C_mat_7, C_mat_7, erodeElement);
		
		export(C_mat_7, "C_mat_7_c");
//		Mat drawing = Mat.zeros(this.source_image.size(), this.source_image.type()); //create a all zeros image
//    	Random rng = new Random();
		List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
    	Imgproc.findContours(C_mat_7.clone(), contour, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
    	
    	for (int i = 0; i < contour.size(); i++) {
    	    Scalar color = new Scalar(255,255,0);
    	    Imgproc.drawContours(this.source_image, contour, i, color, 2, 8, hierarchy, 0, new Point(0, 0));
    	} //end of for (int i = 0; i < contour.size(); i++)
    	export(this.source_image, "drawing_c");
    	return this.source_image;
	}
    
	public Mat DrawEdge(Mat src) {
		Mat tempImg = src.clone();
		Mat theColor = new Mat();
		Mat yCbCrImage = new Mat();
		
		List<Mat> yCbCrChannels = new ArrayList<Mat>();
		Imgproc.cvtColor(tempImg, yCbCrImage, Imgproc.COLOR_BGR2YCrCb);//2HLS 3HSV 4LUV 5YUV 6YCBCR
		Core.split(yCbCrImage, yCbCrChannels);
		
		
//		Mat gray = new Mat();
//		this.export(yCbCrChannels.get(0), "grayCrBin0");
//		this.export(yCbCrChannels.get(1), "grayCrBin1");
//		this.export(yCbCrChannels.get(2), "grayCrBin2");
		
//		gray= yCbCrChannels.get(0).clone();
		Mat calcHis = new Mat();
		List<Mat> matList = new ArrayList<Mat>();			
		matList.add(yCbCrChannels.get(2));
//		this.export(yCbCrChannels.get(2), "grayCrBin2_norm");
		MatOfFloat ranges=new MatOfFloat(0,255);
		MatOfInt hstSize = new MatOfInt(255);
		Imgproc.calcHist(	matList, 
	    	                new MatOfInt(0), 
	    	                new Mat(), 	
	    	                calcHis , 
	    	                hstSize , 
	    	                ranges);

//		Core.normalize(calcHis, calcHis,0, 255,Core.NORM_MINMAX, -1);

		/////////////////////////////////// get image_th ///////////////////
		float image_th = 0;
//		float image_th_next = 0;
		float image_big_temp = 0;
		float[] data = {0};
		int image_big1 = 0;
		int image_big2 = 0;
//		int wave_cnt = 0;
		for(int i = 0 ; i < 255 ; i ++){
			calcHis.get(i, 0, data);
			image_th = data[0];
			if(image_th > image_big_temp){image_big_temp = image_th ;image_big1 = i;}

			
		}
		System.out.println("image_th = " + image_big_temp + ",image_big1 = " + image_big1);
		
		int result_image = 0;
//		while(image_big_temp > 100 && image_big1 != 0){
//
//			image_big_temp = 0;
//			int temp = image_big1;
////			image_big1 = image_big2;
//			for(int i = 0 ; i < image_big1 ; i ++){
//				calcHis.get(i, 0, data);
//				image_th = data[0];
//				if(image_th >= image_big_temp){image_big_temp = image_th ;image_big2 = i;}
//
//				
//			}
//			System.out.println("image_th = " + image_big_temp + ",image_big2 = " + image_big2 + ",image_big1 = " + image_big1 );
//			System.out.println("image_big2 = " + image_big2);
//			result_image = image_big1;
//			image_big1 = image_big2;
//		}
//		System.out.println("result_image = " +result_image);
//		Imgproc.threshold(yCbCrChannels.get(2).clone(), theColor, image_big1, 255, Imgproc.THRESH_BINARY_INV);
//		
		
		
//		while(image_big_temp > 100 && image_big1 != 0){
		
			image_big_temp = 0;
			int temp = 0;
//			image_big1 = image_big2;
			for(int i = image_big1 ; i > 0 ; i --){
				Imgproc.threshold(yCbCrChannels.get(2).clone(), theColor, i, 255, Imgproc.THRESH_BINARY_INV);
//				Core.countNonZero(theColor);
				if(Math.abs(Core.countNonZero(theColor)-temp) < (src.width()*src.height())/250 ){
					result_image = i;
					break;
				}
				temp = Core.countNonZero(theColor);
			}
			System.out.println("image_th = " + image_big_temp + ",image_big2 = " + image_big2 + ",image_big1 = " + image_big1 );
			System.out.println("image_big2 = " + image_big2);
			System.out.println("(src.width()*src.height())/250 = "+(src.width()*src.height())/250);
			image_big1 = image_big2;
//		}
		System.out.println("result_image = " +result_image);
		Imgproc.threshold(yCbCrChannels.get(2).clone(), theColor, result_image, 255, Imgproc.THRESH_BINARY_INV);
		
		
		return theColor;
	}
	
    public List<Mat> findKoi(int stream) {
//    	Mat DrawCon = this.DrawCon(this.source_image) ;
//    	Mat tempImage_pre = this.detectColor(this.source_image, HSV_BLUE_MIN, HSV_BLUE_MAX,stream);
    	Mat tempImage_pre_2 = this.DrawEdge(this.source_image);
    	this.export(tempImage_pre_2, "tempImage_pre_2_"+stream);
    	Mat tempImage = this.RemoveEdgeCut(tempImage_pre_2);
    	this.export(tempImage, "tempImage_"+stream);
    	Mat fish_no_background = this.removeColor(this.source_image ,tempImage);
    	
    	
    	
    	
    	List<Mat> sigleFishList = new ArrayList<Mat>();

    	//==Find contours ========================================================================================
    	List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
    	Mat hierarchy = new Mat();
//    	Imgproc.cvtColor(fish_no_background, tempImage, Imgproc.COLOR_BGR2GRAY);
//    	this.export(fish_no_background, "fish_no_background");
    	Imgproc.findContours(tempImage, contour, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);

    	//=== Draw contours======================================================================================
    	Mat drawing = Mat.zeros(this.source_image.size(), this.source_image.type()); //create a all zeros image
    	Random rng = new Random();
    	for (int i = 0; i < contour.size(); i++) {
    	    Scalar color = new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256));
    	    Imgproc.drawContours(drawing, contour, i, color, 2, 8, hierarchy, 2, new Point(0, 0));
    	} //end of for (int i = 0; i < contour.size(); i++)

    	this.export(drawing, "coutours");

    	//=== find boundingRect =================================================================================
    	int index = 0;
    	List<Double> theArea = new ArrayList<Double>();
    	List<Rect> theboundingRect = new ArrayList<Rect>();
    	List<Mat> fishImage = new ArrayList<Mat>();
    	List<Mat> binFishImage = new ArrayList<Mat>();
    	List<Point> fish_begin_point = new ArrayList<Point>(); 
    	Mat binFish = this.removeColor(this.source_image,tempImage);
    	Imgproc.cvtColor(binFish, binFish, Imgproc.COLOR_BGR2GRAY);
    	Imgproc.threshold(binFish, binFish, 10, 255, Imgproc.THRESH_BINARY);

    	Mat Kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15));
    	Imgproc.dilate(binFish, binFish, Kernel);
    	Imgproc.erode(binFish, binFish, Kernel);
//    	this.export(binFish, "bin_fish");

    	Mat ROI = new Mat();
    	Mat drawBR = drawing.clone();
    	double minAreaLim = this.source_image.cols() * this.source_image.rows() * 0.002;
    	double maxAreaLim = this.source_image.cols() * this.source_image.rows() * 0.95;
    	int contourCnt = 0;
//    	int more = 0;
    	for (int i = 0; i < hierarchy.cols(); i++) {
    	    for (int j = 0; j < hierarchy.rows(); j++) {
    		index = j * hierarchy.rows() + i;

    		double tempArea = Imgproc.contourArea(contour.get(index));
    //TODO
    		if ((tempArea > minAreaLim) && (tempArea < maxAreaLim)) {
    			theboundingRect.add(Imgproc.boundingRect(contour.get(index)));
    		    Imgproc.rectangle(drawBR, theboundingRect.get(contourCnt).tl(),
    			    theboundingRect.get(contourCnt).br(), new Scalar(255, 0, 255));
    		    
//    		    this.export(drawBR, "source_image_"+contourCnt);
    		    if( (Math.abs(theboundingRect.get(contourCnt).tl().x)>2)&&
    			    	(Math.abs(theboundingRect.get(contourCnt).tl().y)>2)&&
    			    	(Math.abs(theboundingRect.get(contourCnt).br().x-source_image.cols())>2)&&
    			    	(Math.abs(theboundingRect.get(contourCnt).br().y-source_image.rows())>2))
    		    {
    		    	theArea.add(tempArea);
    			    ROI = fish_no_background.submat(theboundingRect.get(contourCnt));
    		    	fishImage.add(ROI);
//    		    	export(ROI, "ROIIII");
    		    	fish_begin_point.add(theboundingRect.get(contourCnt).tl());
//    		    this.putTextAndExport(ROI, "fish_contour_" + index,
//    			    theArea.get(more).toString());
    		    
    		    ROI = binFish.submat(theboundingRect.get(contourCnt));
//    		    more++;
    		    binFishImage.add(ROI);
    		    }

    		    
    		    contourCnt++;
    		}

    	    }
    	}
    	
//    	this.export(drawBR, "coutours and bounding");
    	//=== Rotate fish head to bottom side and fill the background ===

    	List<Mat> result = new ArrayList<Mat>();
    	List<Mat> org_tempList = new ArrayList<Mat>();
    	
    	for (int k = 0; k < fishImage.size(); k++) {
//    	    this.export(fishImage.get(k), "fishImage_where_"+k);
    	    List<Mat> tempList = this.rotateFishHead2Bottom(fishImage.get(k),fish_begin_point.get(k));
    	    Mat tempRotate = tempList.get(0);
    	    sigleFishList.add(tempRotate);
    	    
    	    org_tempList.add(tempList.get(1));
//    	    this.export(sigleFishList.get(k), "fill_" + k);
    		result.add(sigleFishList.get(k));

    	}
    	
    	for (int k = 0; k < fishImage.size(); k++) {
    	    Mat tempRotate = org_tempList.get(k);
//    		this.export(tempRotate, "org_Fish_" + k);
    		result.add(tempRotate);
    	}
    	
    	return result;
    }//end of 
    
    public void ReleaseMat() {
    	this.source_image.release();
        }//end of void setSourceImage(Mat newSource)
    private int getLargestCotour(Mat image, List<MatOfPoint> contour, Mat hierarchy) {
	Mat tempImage = image.clone();
	int contourIndex = 0;
	double max_area = 0, area = 0;

	Imgproc.findContours(tempImage, contour, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_TC89_KCOS);

	//---calculate area and find the biggest-------------
	if (contour.size() > 1) {
	    for (int i = 0; i < contour.size(); i++) {
		area = (Imgproc.contourArea(contour.get(i)));
		if (max_area < area) {
		    contourIndex = i;
		    max_area = area;
		}
	    }
	} else if (contour.size() == 0) {
	    System.out.println("no any contour !");
	    return 0xff;
	} //end of if (contour.size() > 1)

	return contourIndex;
    }
}//end of class koi_id
