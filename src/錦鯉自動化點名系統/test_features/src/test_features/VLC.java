//********************************************************
//**  This code is write for Koi project             	**
//**  Part     : koi segmentation VLC Decoder & Encoder	**
//**  Version  : 1.0                                    **
//**  detail   : 將二値化影像延展成254*1000，編碼成254組VLC.   **
//**             將254組VLC解碼成254*1000的影像.            **
//**  Engineer : Eric                                	**
//**  Generate Date : 2016/11/18                       	**
//**  Release Date  : 2016/11/18                      	**
//********************************************************
package test_features;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

public class VLC{
	public void VLC_Decoder(Mat src, String[] vlc_str){
		Mat vlc = new Mat(new Size(254,1000), CvType.CV_16UC1);
		Imgproc.resize(src, vlc, new Size(254,1000));
		Core.normalize(vlc, vlc, 0, 255, Core.NORM_MINMAX);
		byte[] data = {0};
		String vlc_string = "";
		int vlc_state = 0;// 0 = black ; 1 = white
		int vlc_W_cnt = 0;
		int vlc_K_cnt = 0;
		for(int i = 0 ; i < 254 ; i ++){
			vlc.get(0, i, data);
			if(data[0] != 0){vlc_state = 1; vlc_string += "0,";}else{vlc_state = 0;}
			for(int j = 0 ; j < 1000 ; j ++){
				vlc.get(j, i, data);
				if(vlc_state == 0){ // vlc_state = black
					if(data[0] != 0){
						vlc_string += vlc_K_cnt + ",";
						vlc_state = 1;
						vlc_K_cnt = 0;
						vlc_W_cnt++;
					}
					else{
						vlc_K_cnt++;
						vlc_state = 0;
						vlc_W_cnt = 0;
					}
				}
				else{ // vlc_state = white
					if(data[0] == 0){
						vlc_string += vlc_W_cnt + ",";
						vlc_W_cnt = 0;
						vlc_state = 0;
						vlc_K_cnt++;
					}
					else{
						vlc_W_cnt++;
						vlc_state = 1;
						vlc_K_cnt = 0;
					}
				}
			}
			// 1000
			vlc.get(999, i, data);
			if(data[0] != 0){
				vlc_string += vlc_W_cnt;
			}
			else{
				vlc_string += vlc_K_cnt;
			}
			vlc_K_cnt = 0;
			vlc_W_cnt = 0;
//			System.out.println(i + "	vlc_string = "+vlc_string);
			vlc_str[i] = vlc_string;
			vlc_string = "";
			
		}
	}//end of public VLC_Decoder()
	
	
	public Mat VLC_Encoder(String[] vlc_string, int width, int height){
		//將經過VLC編碼的 vlc_string[width] 載入，並解碼成 width * height 的影像
		//用 vlc_state 變數做為狀態記錄
		//用 unit_cnt  做為編碼長度的暫存器
		//用 total_cnt 做為已經計數過的unit_cnt的累加器
		
		
		//宣告 width * height 大小的影像
		Mat vlc = new Mat(new Size(width,height), CvType.CV_8UC1);
		int vlc_state = 0;
		int unit_cnt = 0;
		int total_cnt = 0;
		byte[] data = {0};
		
		//以"K"做為描述字元
		String K_string = "K";
		
//		System.out.println("vlc_string.length = " + vlc_string.length);
		
		//執行  width 次數的解碼
		for(int i = 0 ; i < vlc_string.length ; i++){
			//將  csv 格式解成  height 個字碼
			String[] splitted_vlc = vlc_string[i].split(",");
			//取得分割字碼數量
			int total_length = splitted_vlc.length;
			//判斷首字符是否是"K"("K"的意思是'0')
			if(splitted_vlc[0].equals(K_string)){
				//vlc_state = 0 是'0'先
				vlc_state = 0;
			}
			else{
				//vlc_state = 1 是'1'先
				vlc_state = 1;
			}
			//扣掉首字符，所以迴圈從1開始
			for(int j = 1 ; j < total_length ; j++){
				//取得  vlc_string 內的數字
				unit_cnt = Integer.valueOf(splitted_vlc[j]);
				//執行數字長度的迴圈開始填充影像
				for(int k = 0 ; k < unit_cnt ; k++){
					//偵測目前狀態是'0'還是'1'
					if(vlc_state == 0){
						//是'0'的話填入0到新的影像
						data[0] = 0;
						//存放的位置是
						//長 = 已經數過的counter的累加值(total_cnt)再加上目前counter迴圈次數(k)
						//寬 = 目前  width 迴圈次數(i)
						vlc.put(total_cnt+k, i, data);
					}
					else{
						//是'1'的話填入255到新的影像
						data[0] = (byte)255;
						//存放的位置是
						//長 = 已經數過的counter的累加值(total_cnt)再加上目前counter迴圈次數(k)
						//寬 = 目前  width 迴圈次數(i)
						vlc.put(total_cnt+k, i, data);
					}
				}
				//執行填圖完畢後，將vlc狀態反向
				if(vlc_state == 0){vlc_state = 1;}else{vlc_state = 0;}
				//執行填圖完畢後，累加已經數過的counter
				total_cnt += unit_cnt;
			}
			//執行完一行後，將counter的累加值歸0
			total_cnt = 0;
		}
		return vlc;
	}//end of public VLC_Encoder()
}