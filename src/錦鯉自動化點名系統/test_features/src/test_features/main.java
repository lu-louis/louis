package test_features;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import test_features.web_hdfs;


public class main {

	public static void main(String[] args) {

		
        ServerSocket server;
        Socket client;
        InputStream input;

	        try {
	        	while(true)
	        	{

	            server = new ServerSocket(1010);
	            client = server.accept();
	            
	            input = client.getInputStream();
	            String inputString = main.inputStreamAsString(input);


	            System.out.println(inputString);
//	            long starttime = System.currentTimeMillis();
	            
	            
	 //特徵參數擷取==============================================================================================
	    		FeaturesSerialExtractionModule FSEM = new FeaturesSerialExtractionModule();
	    		FSEM.GetAllPicFT(inputString,10);              //(檔案路徑，照片張數)
	            
//	    		long endtime = System.currentTimeMillis();
//	    		 System.out.println("================================================");
//	    		long time = endtime - starttime;
	    		 
//	    		 System.out.println("擷取特徵參數:"+time);
	    		
	 //特徵分類器==============================================================================================	    		
	    		feature_classification fc = new  feature_classification();
	    		fc.classification(FSEM.getFeaturesParam_json(), FSEM.getFeaturesParam_cut_lenght(), 5);
	    		
	    		
     //寫txt==>上傳hdfs========================================================================================    		
	    //將所有特徵參數寫成txt
//	    		feature_write_txt fwt= new feature_write_txt();
//	    		fwt.write(inputString,FSEM.getFeaturesParam_string());
        //將txt上傳hdfs
//	    		web_hdfs conn = new web_hdfs("163.18.49.38", "14000", "admin"); // initial object
//	    		boolean uploadResult = conn.upload("C:/Users/GSLab_member/Autocollection_koi_de64/picture_project/"+inputString+"/"+inputString+".txt", inputString); // 上傳檔案到 webhdfs
//	    		System.out.println(" project: "+ inputString + " upload to hdfs Result : " + uploadResult);

     //call spark============================================================================================
	    		
//	    		socket_client sc=new socket_client();
//	    		sc.socket_client("163.18.49.38",9999,inputString);
	    		
	 //清除所有記憶體
//	    		System.gc();
	            client.close();
	            server.close();
	            }

	        	}

	        catch (Exception e) {
	            e.printStackTrace();
	            
	        }
	}




    public static String inputStreamAsString(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = null;

        while ((line = br.readLine()) != null) {
//            sb.append(line + "\n");
            sb.append(line);
        }

        br.close();
        return sb.toString();
    }

}