package test_features;

import java.io.File;
import java.io.IOException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import test_features.ImageProcessEngine;


public class FeaturesSerialExtractionModule {

    
    private JSONObject all_featuresParam_cut = null;
    private String all_featuresParam = "";
    private String[] AfterSplit;
    /** @return featuresParam       特徵參數(josn)
     */
    
    public void GetAllPicFT(String project,int pic_number)
    {
    	
//    	int i = 1;
    	this.all_featuresParam_cut =  new JSONObject();
    	
//    	String input_filePath = "C:/Users/GSLab_member/Autocollection_koi_de64/picture_project/"+project+"/";
//    	String output_filePath = "C:/Users/GSLab_member/Autocollection_koi_de64/picture_project/"+project+"/"+i+"/";
    	
//    	boolean flag_judge_pic_exist = start(output_filePath,input_filePath+project+"-"+String.valueOf(i)+".jpeg",i) ;
//    	System.out.println("--------------------------"+i+"--------------------------");
//    	while(flag_judge_pic_exist)
//    	{
//    		i ++;
//    		output_filePath = "C:/Users/GSLab_member/Autocollection_koi_de64/picture_project/"+project+"/"+i+"/";
//   		
//    		flag_judge_pic_exist = start(output_filePath,input_filePath+project+"-"+String.valueOf(i)+".jpeg",i) ;
//    		System.out.println("--------------------------"+i+"--------------------------");
//    	}

    	for(int i=1;i <= pic_number;i++)
    	{
        	String input_filePath = "C:/Users/GSLab_member/Autocollection_koi_de64/picture_project/"+project+"/";
        	String output_filePath = "C:/Users/GSLab_member/Autocollection_koi_de64/picture_project/"+project+"/"+i+"/";
        	
    		start(output_filePath,input_filePath+project+"-"+String.valueOf(i)+".jpeg",i) ;

    	}
    	
    	try {
    		
			AfterSplit = all_featuresParam.split("\n");
			
			for (int cut_cnt = 0; cut_cnt < AfterSplit.length; cut_cnt++)
			{	
				
					all_featuresParam_cut.put("koifeaturesParam_"+String.valueOf(cut_cnt),new JSONObject(AfterSplit[cut_cnt]));
	
			}
/* 測試程式
			JSONObject jsonOb = all_featuresParam_cut.getJSONObject("koifeaturesParam_0");
			System.out.println("jsonOb:==========================");	
			System.out.println(jsonOb);
			
			System.out.println("==========================");	
			System.out.println(AfterSplit[0]);
			System.out.println("==========================");	
			System.out.println(AfterSplit[1]);
			System.out.println("==========================");	
			System.out.println(AfterSplit[2]);
*/
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    }
    
    public boolean start(String fileDist,String fileName, int i) 
    {
 
            try {
            	String featuresParam = ImageProcessEngine.Koi_process(fileDist, fileName, i, 0, 0);
            	
				if(featuresParam!="")
					all_featuresParam = all_featuresParam + featuresParam;
				
				
//				all_featuresParam.put("koifeaturesParam_"+String.valueOf(i),featuresParam);
				
				return true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			

    }
    
        public JSONObject getFeaturesParam_json() { return this.all_featuresParam_cut; }
        public int getFeaturesParam_cut_lenght() { return this.AfterSplit.length; }
        public String getFeaturesParam_string() { return this.all_featuresParam; }
  
    
}