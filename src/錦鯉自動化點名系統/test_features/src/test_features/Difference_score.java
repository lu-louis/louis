package test_features;

public class Difference_score {

	public static int avg_adder(int[] src_avg, int[] dst_avg){
		int score = 0;
		int temp_score = 0;
		int total_src = 0;
		int total_dst = 0;
		int temp_src = 0;
		int temp_dst = 0;
		for(int i = 0 ; i < 8 ; i++){
			total_src = total_src + src_avg[i];
			total_dst = total_dst + dst_avg[i];
		}
		for(int i = 0 ; i < 8 ; i++){
			if(src_avg[i] > total_src/8){
				temp_src = src_avg[i] - total_src/8;
			}
			else{
				temp_src = total_src/8 - src_avg[i];
			}
			if(dst_avg[i] > total_dst/8){
				temp_dst = dst_avg[i] - total_dst/8;
			}
			else{
				temp_dst = total_dst/8 - dst_avg[i];
			}
			if(temp_src > temp_dst){
				temp_score =  (temp_src - temp_dst);
			}
			else{
				temp_score =  (temp_dst - temp_src);
			} 
//			System.out.print(" temp_dst = "+temp_dst);
//			System.out.print(" , temp_src = "+temp_src);
			
//			if(temp_score > 100)
//			{score = score + temp_score;}
			if(temp_score > 800)
			{score = score + (5*temp_score);}
			else if(temp_score > 600)
			{score = score + (4*temp_score);}
			else if(temp_score > 400)
			{score = score + (3*temp_score);}
			else if(temp_score > 200)
			{score = score + (2*temp_score);}
			else
			{score = score + temp_score;}
		}
//		System.out.println();
//		System.out.println("score = "+score);
		return score;
	}
	
	public static int hist_adder(int[] src_hist, int[] dst_hist){
		int score = 0;
		int temp_score = 0;
		for(int i = 0 ; i < 254 ; i++){
			if(src_hist[i] > dst_hist[i]){
				temp_score = (src_hist[i] - dst_hist[i]);
			}
			else{
				temp_score = (dst_hist[i] - src_hist[i]);
			}
//			System.out.print("temp_score = "+temp_score);
			
			if(temp_score > 100)
			{score = score + 5*temp_score;}
			else if(temp_score > 80)
			{score = score + 4*temp_score;}
			else if(temp_score > 60)
			{score = score + 3*temp_score;}
			else if(temp_score > 40)
			{score = score + 2*temp_score;}
			else if(temp_score > 20)
			{score = score + temp_score;} 
		}
		
//		System.out.println();
//		System.out.println("score = "+score);
		return score;
	}
}
