//package Image.Processing;

//********************************************************
//**  This code is write for Koi project                **
//**  Part     : koi segmentation                       **
//**  Version  : 2.10                                   **
//**  detail   : 切割錦鯉斑紋-紅色區塊.                      **
//**  Engineer : James                                  **
//**  Generate Date : 2016/10/27                        **
//**  Release Date  : 2016/11/12                        **
//********************************************************
package test_features;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

public class segmentation
{
    /***** using default parameter constructor *****/
    public segmentation(){}

    /***** TODO segRed *****/
    public void segRed(Mat src, List<Mat> dst)
    {
        Mat blur_src = new Mat();
        Mat Gray_src = new Mat();
        Mat maskKoi  = new Mat();

        List<Mat> planes = new ArrayList<Mat>();
        Mat r_b = new Mat();
        Mat r_g = new Mat();

        Scalar scalar_gray_1 = null;
        Scalar scalar_r_g_1  = null;
        Scalar scalar_r_b_1  = null;
        Scalar scalar_gray_2 = null;
        Scalar scalar_r_g_2  = null;
        Scalar scalar_r_b_2  = null;
        Scalar scalar_gray_3 = null;
//        Scalar scalar_g_3    = null;
//        Scalar scalar_b_3    = null;

        Mat maskRG1 = new Mat();
        Mat maskRB1 = new Mat();
        Mat maskRG2 = new Mat();
        Mat maskRB2 = new Mat();
        Mat maskG3  = new Mat();
        Mat maskB3  = new Mat();

        Mat maskBrighterRed = new Mat();
        Mat maskDarkerRed   = new Mat();
        Mat _maskRed        = new Mat();
        Mat maskRed         = new Mat();
        Mat maskPink        = new Mat();
        Mat maskRealRed     = new Mat();

        Mat maskKoiBrighter = new Mat();
        Mat _maskDark       = new Mat();
        Mat maskDark        = new Mat();

        Mat kernel_close = Imgproc.getStructuringElement(Imgproc.CV_SHAPE_RECT, new Size(5, 5));

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat Contours_maskRealRed  = new Mat();
        Mat hierarchy             = new Mat();
        Mat Contours_src          = new Mat();

        Mat DarkImg      = new Mat();
        Mat seg_red      = new Mat();
        Mat seg_real_red = new Mat();

        /** @param  blur_src                原圖模糊化
        /** @param  Gray_src                原圖灰階化
        /** @param  maskKoi                 錦鯉遮罩
        /** @param  planes                  分離通道
        /** @param  r_b                     R通道減B通道
        /** @param  r_g                     R通道減G通道
        /** @param  scalar_gray_1           比較門檻1(灰階)
        /** @param  scalar_r_g_1            比較門檻1(R通道減B通道)
        /** @param  scalar_r_b_1            比較門檻1(R通道減G通道)
        /** @param  scalar_gray_2           比較門檻2(灰階)
        /** @param  scalar_r_g_2            比較門檻2(R通道減B通道)
        /** @param  scalar_r_b_2            比較門檻2(R通道減G通道)
        /** @param  scalar_gray_3           比較門檻3(灰階)
        /** @param  scalar_g_3              比較門檻3(G通道)
        /** @param  scalar_b_3              比較門檻3(B通道)
        /** @param  maskRG1                 亮紅RG遮罩
        /** @param  maskRB1                 亮紅RB遮罩
        /** @param  maskRG2                 暗紅RG遮罩
        /** @param  maskRB2                 暗紅RB遮罩
        /** @param  maskG3                  粉紅G遮罩
        /** @param  maskB3                  粉紅B遮罩
        /** @param  maskBrighterRed         亮紅遮罩
        /** @param  maskDarkerRed           暗紅遮罩
        /** @param  _maskRed                紅色遮罩
        /** @param  maskRed                 紅色遮罩(收斂)
        /** @param  maskPink                粉紅遮罩
        /** @param  maskRealRed             真紅色遮罩
        /** @param  maskKoiBrighter         錦鯉亮色遮罩
        /** @param  _maskDark               暗色遮罩1
        /** @param  maskDark                暗色遮罩2
        /** @param  kernel_close            收斂核心
        /** @param  contours                輪廓點
        /** @param  Contours_maskRealRed    真紅色遮罩(contours專用)
        /** @param  hierarchy               ?
        /** @param  Contours_src            原圖紅色輪廓圖
        /** @param  DarkImg                 錦鯉暗色區
        /** @param  seg_red                 紅色分割圖
        /** @param  seg_real_red            真紅色分割圖

        /***** begin *****/

        // blur
        Imgproc.blur(src, blur_src, new Size(5, 5));

        // Step.1 Seg BrighterRed

        // Gray
        Imgproc.cvtColor(blur_src, Gray_src, Imgproc.COLOR_BGR2GRAY);

        // maskKoi
        Imgproc.threshold(Gray_src, maskKoi, 1, 255, Imgproc.THRESH_BINARY);

        // Split
        // 分離通道
        Core.split(blur_src, planes);

        // Subtract
        // R通道減B通道、R通道減G通道
        Core.subtract(planes.get(2), planes.get(0), r_b);
        Core.subtract(planes.get(2), planes.get(1), r_g);

        // Mean
        // 平均亮度
        scalar_gray_1 = Core.mean(Gray_src, maskKoi);
        scalar_r_g_1  = Core.mean(r_g, maskKoi);
        scalar_r_b_1  = Core.mean(r_b, maskKoi);

        // 門檻過低補正
        if( (scalar_r_g_1.val[0] < (scalar_gray_1.val[0]/3)) && (scalar_r_b_1.val[0] < (scalar_gray_1.val[0]/3)) )
        { scalar_r_g_1.val[0] = scalar_gray_1.val[0]/3; scalar_r_b_1.val[0] = scalar_gray_1.val[0]/3;}

        // --- 以下註解為測試門檻值用 ---
//        System.out.println("scalar_gray_1 = " + scalar_gray_1.val[0]);
//        System.out.println("scalar_r_g_1  = " + scalar_r_g_1.val[0] );
//        System.out.println("scalar_r_b_1  = " + scalar_r_b_1.val[0] );

        // Compare
        // 取得 亮紅RG遮罩、亮紅RB遮罩
        Core.compare(r_g, new Scalar((double)scalar_r_g_1.val[0]), maskRG1, Core.CMP_GT);
        Core.compare(r_b, new Scalar((double)(scalar_r_b_1.val[0] * 0.5)), maskRB1, Core.CMP_GT);

        // Multiply
        // 取得 亮紅遮罩
        Core.multiply(maskRG1, maskRB1, maskBrighterRed);

        // Step.2 Get DarkImg

        // Compare
        // 取得 錦鯉亮色遮罩
        Core.compare(Gray_src, new Scalar(scalar_gray_1.val[0]), maskKoiBrighter, Core.CMP_GT);

        // Subtract
        // 暗色遮罩 = 錦鯉遮罩 - 錦鯉亮色遮罩 - 亮紅遮罩
        Core.subtract(maskKoi, maskKoiBrighter, _maskDark);
        Core.subtract(_maskDark, maskBrighterRed, maskDark);

        // 取得 錦鯉暗色區
        blur_src.copyTo(DarkImg, maskDark);

        // Step.3 Seg DarkRed

        // Split
        // 分離通道
        Core.split(DarkImg, planes);

        // Subtract
        // R通道減B通道、R通道減G通道
        Core.subtract(planes.get(2), planes.get(0), r_b);
        Core.subtract(planes.get(2), planes.get(1), r_g);

        // Mean
        // 平均亮度
        scalar_gray_2 = Core.mean(Gray_src, maskDark);
        scalar_r_g_2  = Core.mean(r_g, maskDark);
        scalar_r_b_2  = Core.mean(r_b, maskDark);

        // --- 以下註解為測試門檻值用 ---
//        System.out.println("scalar_r_g_2  = " + scalar_r_g_2.val[0]);
//        System.out.println("scalar_r_b_2  = " + scalar_r_b_2.val[0]);

        // 門檻過低補正
        if( (scalar_r_g_2.val[0] < (scalar_gray_2.val[0]/3)) && (scalar_r_b_2.val[0] < (scalar_gray_2.val[0]/3)) )
        { scalar_r_g_2.val[0] = scalar_gray_2.val[0] /3; scalar_r_b_2.val[0] = scalar_gray_2.val[0] /3;}

        if( (scalar_r_g_2.val[0] < 32) && (scalar_r_b_2.val[0] < 32) )
        { scalar_r_g_2.val[0] = 32.0; scalar_r_b_2.val[0] = 64.0;}

        // --- 以下註解為測試門檻值用 ---
//        System.out.println("scalar_gray_2 = " + scalar_gray_2.val[0]);
//        System.out.println("scalar_r_g_2  = " + scalar_r_g_2.val[0]);
//        System.out.println("scalar_r_b_2  = " + scalar_r_b_2.val[0]);

        // Compare
        // 取得 暗紅RG遮罩、暗紅RB遮罩
        Core.compare(r_g, new Scalar((double)scalar_r_g_2.val[0]), maskRG2, Core.CMP_GT);
        Core.compare(r_b, new Scalar((double)(scalar_r_b_2.val[0] * 0.5)), maskRB2, Core.CMP_GT);

        // Multiply
        // 取得 暗紅遮罩
        Core.multiply(maskRG2, maskRB2, maskDarkerRed);

        // Step.4 Get seg_red

        // Add
        // 取得 紅色遮罩
        Core.add(maskBrighterRed, maskDarkerRed, _maskRed);

        // CLOSE
        // 紅色遮罩收斂
        Imgproc.morphologyEx(_maskRed, maskRed, Imgproc.MORPH_CLOSE, kernel_close);

        // 取得 紅色分割圖
        blur_src.copyTo(seg_red, maskRed);

        // Step.4 Remove pink

        // Split
        // 分離通道
        Core.split(seg_red, planes);

        // Mean
        // 平均亮度
        scalar_gray_3 = Core.mean(Gray_src, maskRed);
//        scalar_g_3    = Core.mean(planes.get(1), maskRed);
//        scalar_b_3    = Core.mean(planes.get(0), maskRed);

        // --- 以下註解為測試門檻值用 ---
//        System.out.println("scalar_gray_3 = " + scalar_gray_3.val[0]);
//        System.out.println("scalar_g_3 = " + scalar_g_3.val[0]);
//        System.out.println("scalar_b_3 = " + scalar_b_3.val[0]);

        // Compare
        // 取得 粉紅G遮罩、粉紅B遮罩
        Core.compare(planes.get(1), new Scalar((double)scalar_gray_3.val[0]), maskG3, Core.CMP_GT);
        Core.compare(planes.get(0), new Scalar((double)scalar_gray_3.val[0]), maskB3, Core.CMP_GT);

        // Multiply
        // 取得 粉紅遮罩
        Core.multiply(maskB3, maskG3, maskPink);

        // Subtract
        // 真紅色遮罩 = 紅色遮罩 - 粉紅遮罩
        Core.subtract(maskRed, maskPink, maskRealRed);

        // Step.5 Get RealRed

        // 取得 真紅色分割圖
        src.copyTo(seg_real_red, maskRealRed);

        // Step.6 Draw contours

        // FindContours
        Contours_maskRealRed = maskRealRed.clone();
        Imgproc.findContours(Contours_maskRealRed, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        // DrawContours
        Contours_src = src.clone();
        for(int i = 0; i< contours.size(); i++)
        { Imgproc.drawContours(Contours_src, contours, i, new Scalar(255, 0, 0), 1); }

        // Step.7 Output
        // --- 以下註解都是測試用圖 ---
//        dst.add(blur_src);        // 0
//        dst.add(Gray_src);        // 1
//        dst.add(maskKoi);         // 2
//
//        dst.add(maskRG1);         // 3
//        dst.add(maskRB1);         // 4
//        dst.add(maskBrighterRed); // 5
//
//        dst.add(maskKoiBrighter); // 6
//        dst.add(_maskDark);       // 7
//        dst.add(maskDark);        // 8
//        dst.add(DarkImg);         // 9
//
//        dst.add(maskRG2);         // 10
//        dst.add(maskRB2);         // 11
//        dst.add(maskDarkerRed);   // 12
//
//        dst.add(_maskRed);        // 13
//        dst.add(maskRed);         // 14
//        dst.add(seg_red);         // 15
//
//        dst.add(maskG3);          // 16
//        dst.add(maskB3);          // 17
//        dst.add(maskPink);        // 18

        dst.add(maskRealRed);     // 19(Binary)
        dst.add(seg_real_red);    // 20(COLOR)
        dst.add(Contours_src);    // 21(Contours)
    } // end segRed

} // end class