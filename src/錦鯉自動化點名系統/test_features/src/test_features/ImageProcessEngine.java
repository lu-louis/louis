//********************************************************
//**  This code is write for Koi project             	**
//**  Part     : koi identification                  	**
//**  Version  : 2.43                                   **
//**  detail   : 整合紅色區域跟黑色區域的segmentation.        **
//**             修復WK_QR取得re_get_mat會有二値化錯誤的BUG.  **
//**  Engineer : Eric                                	**
//**  Generate Date : 2016/11/21                       	**
//**  Release Date  : 2016/12/02                     	**
//********************************************************
package test_features;


import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import org.opencv.core.MatOfPoint;

import java.util.ArrayList;
import java.util.List;
import java.awt.image.*;

import javax.imageio.ImageIO;




//import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.OutputStreamWriter;





import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
//import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;



public class ImageProcessEngine{
	static public String ImageProcess_Version = "version 2.43";
	static public Mat image;
	public ImageProcessEngine()
	{}//end of public koi_id()
	public static BufferedImage mat2Img(Mat mat) {
        BufferedImage image = new BufferedImage(mat.width(), mat.height(), BufferedImage.TYPE_3BYTE_BGR);
        WritableRaster raster = image.getRaster();
        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
        byte[] data = dataBuffer.getData();
        mat.get(0, 0, data);
        return image;
    }
    public static Mat img2Mat(BufferedImage image)
    {
		byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
		mat.put(0, 0, data);
		return mat;
    }
    public static void export(Mat image, String fileName) {
		try {
		    Imgcodecs.imwrite("koi/koi_2/" + fileName + ".bmp", image);
		} catch (Exception e) {
		    // TODO: handle exception
		    System.out.println("file write error!");
		    System.out.println(e.getMessage());
		}
    }//end of void Export(String fileName)

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    static int cnt;
    //TODO JSONObject
    public static String Koi_process(String output_path,String input_path ,int stream_no, int is_old, int Yth) throws IOException,JSONException {
    //int Yth)
    	
    	JSONObject NewDataKoi = new JSONObject();
    	String JSON_string = "";
	    
	    cnt = stream_no;
    	int begin_x;
    	int begin_y;
    	int range_x;
    	int range_y;
    	int koi_size ;
//    	int stream_no = 0;
    	List<int[][]> Koi_process_result = new ArrayList<int[][]>();
//    	byte[] data = {0,0,0};
//    	byte[] data_Black = {0,0,0};
//    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
//    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	int[][] avg_hist_area_result = new int[20][8] ;
	    int[][] P_hist_reg_Rarea_result = new int[20][256] ;
	    int[][] P_hist_reg_WKarea_result = new int[20][256] ;
	    int[][] P_hist_reg_R_result = new int[20][256] ;
	    int[][] P_hist_reg_G_result = new int[20][256] ;
	    int[][] P_hist_reg_B_result = new int[20][256] ;
    	int[] koi_kind = null;
    	int[] get_R_cnt = null;
    	int[] get_WK_cnt = null;
    	int[][] get_R_area=null;
    	int[][] get_R_size=null;
    	int[][] get_WK_area=null;
    	int[][] get_WK_size=null;
//    	int[][] area_div ;
//    	int contour_area = 0;
    	int contour_Rarea = 1;
    	int contour_WKarea = 2;
    	int contour_R = 3;
//    	int contour_G = 4;
//    	int contour_B = 5;
//    	int hisg_x,hisg_y;
    	int kind_others = 0;
    	int kind_Showa_Sanshoku = 1;
    	int kind_Taisho_Sanshoku = 2;
    	int kind_Kohaku = 3;
    	int kind_Tancho = 4;
    	int kind_Bekko = 5;
    	int [] fishId = {
    		kind_others,			//0
    		kind_Showa_Sanshoku,	//1
    		kind_Taisho_Sanshoku,	//2
    		kind_Kohaku,			//3
    		kind_Tancho,			//4
    		kind_Bekko				//5
    	};
    	String [] fishId_name = {
    		"kind_others",			//0
    		"kind_Showa_Sanshoku",	//1
    		"kind_Taisho_Sanshoku",	//2
    		"kind_Kohaku",			//3
    		"kind_Tancho",			//4
    		"kind_Bekko"				//5
    	};
//    	long startTime ;
//    	startTime = System.currentTimeMillis();
    	
    	System.out.print("Process Begin !   ");
//    	System.out.println("ImageProcess_Version is " + ImageProcess_Version);
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        try {
    	CutterKernel koi = new CutterKernel();
    	koi.GetImage(input_path);
    	List<Mat> Kois = koi.findKoi(stream_no);
    	
    	if(Kois.size()/2 >= 20){koi_size = 20;}
    	else{koi_size = Kois.size()/2;}
    	JSONObject Cut_N = new JSONObject();
    	koi.putTextAndExport(koi.source_image,"Source_Image", "Source_Image");
    	image = koi.source_image;
//System.out.println("Koi cut finish, Using Time = "+ (System.currentTimeMillis()-startTime)+" ms");
			String Cut_no="";
		    
	        int[][][] P_hist_reg;
	        P_hist_reg = new int[10][20][256];
	        long[][] avg_hist_area;
	        avg_hist_area= new long[koi_size][8];
	        koi_kind = new int[koi_size];
	        get_R_cnt = new int[koi_size];
	        get_WK_cnt = new int[koi_size];
	
//	        area_div = new int[koi_size][256];
	        get_R_area = new int[koi_size][256];
	        get_R_size = new int[koi_size][256];
	        get_WK_area = new int[koi_size][750];
	        get_WK_size = new int[koi_size][750];
		    
	        long[] rgb_th_erodil_Image_area;
	        rgb_th_erodil_Image_area= new long[koi_size];
	        
	        //"{\"Cut_"+i+"\":{\"Name\":\"MichaelChan\",\"Email\":\"XXXX@XXX.com\",\"Phone\":[1234567,0911123456]}}";}
	    	
//		    Object jsonOb = json.getJSONObject("Cut_0").getJSONObject("param").getJSONArray("sd1");

	        
	        
	        
	        
	        System.out.println("Kois.fish.size() = "+koi_size);
//	        BufferedWriter fw = null;
//	        File txt_file = new File(output_path+"KOI_feature.txt");//+"_f"+fish_no_1+"s"+stream_no_1+"_f"+fish_no_2+"s"+stream_no_2+".TXT");
//	        fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(txt_file, false), "UTF-8"));
//	        fw.append(""+koi_size+"|");
//	        fw.newLine();
	    if (koi_size>0 && koi_size<20){
	    	int[] K_get_min = new int[20];
		    for(int i = 0 ; i <= koi_size ; i++){ K_get_min[i] = 200;}
	        for(int fish_no = 0; fish_no < koi_size && fish_no < 20 ; fish_no ++){
	        	
	        	
	        	
//	        	JSONObject Cut_JSON = new JSONObject();
	        	//Cut_no = "Db_"+(stream_no+1);
	        	Cut_no = "Cut_"+(fish_no+1);
		        Mat mat;
		    	mat = Kois.get(fish_no);
		    	
				
		    	BufferedImage bufferedImage = mat2Img(mat);
		        begin_x =  0;//Full_resize4 250;//Full_resize3 240;//0;//200;
		        begin_y =  0;//Full_resize4 140;//Full_resize3 260;//0;//100;
		        range_x =  mat.width();//Full_resize4 100;//Full_resize3 100;//w1;//100;//w1;
		        range_y =  mat.height();//Full_resize4 250;//Full_resize3 250;//h1;//250;//h1;

		        BufferedImage hsvImage = new BufferedImage(range_x, range_y, BufferedImage.TYPE_3BYTE_BGR);	      
		        BufferedImage hsv_r_get_Image = new BufferedImage(mat.width(),mat.height(), BufferedImage.TYPE_3BYTE_BGR);
		        BufferedImage rgb_th_WK_Image = new BufferedImage(mat.width(),mat.height(), BufferedImage.TYPE_3BYTE_BGR);
		        Mat mat_R = new Mat(hsvImage.getHeight(), hsvImage.getWidth(), CvType.CV_8UC3);
		        Mat mat_WK = new Mat(hsvImage.getHeight(), hsvImage.getWidth(), CvType.CV_8UC3);
		        Mat RGB_mat = new Mat(mat.size(),mat.type());
		    	RGB_mat = mat.clone();
		    	List<Mat> BGRChannels = new ArrayList<Mat>();
		    	
		    	Core.split(RGB_mat, BGRChannels);
		        int b_th = 750;
	  	      	int cut_th = 40;
			
				List<MatOfPoint> contours_pre_R = new ArrayList<MatOfPoint>();
				List<MatOfPoint> contours_pre_WK = new ArrayList<MatOfPoint>();
				Mat dest_R = new Mat();
				Mat drawCon_R = new Mat();
				Mat dest_WK = new Mat();
				Mat drawCon_WK = new Mat();
//get R value and WK value
				for(int i = 0 ; i < range_x ; i++){
				    for(int j = 0 ; j < range_y; j++){
				    	int hsv_rgb = bufferedImage.getRGB(i+begin_x, j+begin_y);
				    	int rgb_R = (hsv_rgb>>16)&0xff;
				        int rgb_G = (hsv_rgb>>8)&0xff;
				        int rgb_B = (hsv_rgb)&0xff;
			    		if(((rgb_R) - (rgb_G)>0)&(rgb_R) - (rgb_B)>0){
							byte[] data_buf = {(byte)(rgb_B),(byte)(rgb_G),(byte)(rgb_R)};
							mat_R.put(j, i, data_buf);
						}
			    		else{
			    			mat_R.put(j, i, data_Background);
			    		}
				    }
				}
				for(int i = 0 ; i < range_x ; i++){
				    for(int j = 0 ; j < range_y; j++){
				    	int hsv_rgb = bufferedImage.getRGB(i+begin_x, j+begin_y);
			          	int rgb_R = (hsv_rgb>>16)&0xff;
				        int rgb_G = (hsv_rgb>>8)&0xff;
				        int rgb_B = (hsv_rgb)&0xff;
				    	if(((rgb_R) + (rgb_G) + (rgb_B)) < b_th &&
				    			(rgb_R) - (rgb_G)	< cut_th &&
			          			(rgb_R) - (rgb_B)   < cut_th &&
			          			(rgb_G) - (rgb_R)	< cut_th &&
			          			(rgb_G) - (rgb_B)   < cut_th &&
			          			(rgb_B) - (rgb_R)   < cut_th &&
			          			(rgb_B) - (rgb_G)   < cut_th
			          			) {	
							byte[] data_buf = {(byte)(rgb_B),(byte)(rgb_G),(byte)(rgb_R)};
							mat_WK.put(j, i, data_buf);
						}
			    		else{
			    			mat_WK.put(j, i, data_Background);
			    		}
				    }
				}

			
				Core.inRange(mat_R, new Scalar(1, 1, 1), new Scalar(255, 255, 255), dest_R);
				Imgproc.findContours(dest_R, contours_pre_R, drawCon_R, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				Core.inRange(mat_WK, new Scalar(1, 1, 1), new Scalar(255, 255, 255), dest_WK);
				Imgproc.findContours(dest_WK, contours_pre_WK, drawCon_WK, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				export(mat_WK, "K_mat_WK_"+fish_no);
				export(mat_R, "R_mat_R_"+fish_no);
			
				//取得最大contour的contour編號與大小
				int max_area_pre_R = 0;
				int max_area_pre_WK = 0;
				for(int i=0; i< contours_pre_R.size();i++){
				Rect rect = Imgproc.boundingRect(contours_pre_R.get(i));
					if((int)rect.area() < 25){
						contours_pre_R.remove(i);
						i--;
				    }
				    if((int)rect.area() > max_area_pre_R){
				    	max_area_pre_R = (int) rect.area();
				    }
				}
				for(int i=0; i< contours_pre_WK.size();i++){
				Rect rect = Imgproc.boundingRect(contours_pre_WK.get(i));
					if((int)rect.area() < 25){
						contours_pre_WK.remove(i);
						i--;
				    }
				    if((int)rect.area() > max_area_pre_WK){
				    	max_area_pre_WK = (int) rect.area();
				    }
				}
//System.out.println("get R & WK contour finish, Using Time = "+ (System.currentTimeMillis()-startTime)+" ms");
//				System.out.println("contours_pre_R.size() = "+contours_pre_R.size()+" , contours_pre_WK.size() = "+contours_pre_WK.size());
		        int bb=0;
		        int largest_R = 0;
		        int largest_WK = 0;
				int R_part = 0;
			    for(int i = 0 ; i < 256 ; i ++){get_R_size[fish_no][i]=0;get_R_area[fish_no][i]=0;get_WK_size[fish_no][i]=0;get_WK_area[fish_no][i]=0;}
				    get_WK_cnt[fish_no]=200;
			    	get_R_cnt[fish_no]=0;
//------------------------------------------------------------
//-------------- new way to get R & WK value -----------------
//------------------------------------------------------------	    	  
			    for(int i = 0 ; i < range_x ; i++){
			    	for(int j = 0 ; j < range_y; j++){
			          	int hsv_rgb = bufferedImage.getRGB(i, j);
			          	int rgb_R = (hsv_rgb>>16)&0xff;
				        int rgb_G = (hsv_rgb>>8)&0xff;
				        int rgb_B = (hsv_rgb)&0xff;
				        if(((rgb_R) - (rgb_G)>0)&&((rgb_R) - (rgb_B)>0)){
				        	rgb_th_erodil_Image_area[fish_no]++;
//			        	  rgb_th_R_Image
				          	if((rgb_R - rgb_G) > (rgb_R - rgb_B)){
				          		R_part = rgb_R - rgb_B;
					          	if(largest_R < R_part)
					        	{
					        		largest_R = R_part;
					        	}
				          		if( get_R_cnt[fish_no] < R_part )  get_R_cnt[fish_no] = R_part ;
				          	}
				          	else{
				          		R_part = rgb_R - rgb_G;
				          		if(largest_R < R_part)
					        	{
					        		largest_R = R_part;
					        	}
				          		if( get_R_cnt[fish_no] < R_part )  get_R_cnt[fish_no] = R_part ;
				          	}
				          	hsv_r_get_Image.setRGB(i, j, R_part);//16777215 0xffffff
				          	for(int k = 0; k < R_part ; k ++){get_R_area[fish_no][k]++;}//total_new_R_area++;
				        }
				        else{
				        	hsv_r_get_Image.setRGB(i, j, 0);
				        }
			    	}
			    }
	    	  
		    	export(img2Mat(hsv_r_get_Image), "K_get_min_hsv_r_th_Image"+fish_no);
		    	int[][] new_R;
		    	new_R = new int[20][256];
		    	new_R[fish_no] = get_R_area[fish_no].clone();
//System.out.println("new get WK begin time = "+(System.currentTimeMillis()-startTime)+" ms");
				int WK_part = 0;
		    	for(int i = 0 ; i < range_x ; i++){
			        for(int j = 0 ; j < range_y; j++){
			        	int hsv_rgb = bufferedImage.getRGB(i, j);
			          	int rgb_R = (hsv_rgb>>16)&0xff;
				        int rgb_G = (hsv_rgb>>8)&0xff;
				        int rgb_B = (hsv_rgb)&0xff;
			        	if(	
		          			((rgb_R) + (rgb_G) + (rgb_B)) < b_th &&
		          			(rgb_R) - (rgb_G)	< cut_th &&
		          			(rgb_R) - (rgb_B)   < cut_th &&
		          			(rgb_G) - (rgb_R)	< cut_th &&
		          			(rgb_G) - (rgb_B)   < cut_th &&
		          			(rgb_B) - (rgb_R)   < cut_th &&
		          			(rgb_B) - (rgb_G)   < cut_th)
						{
			        		bb = (768-(rgb_R + rgb_G + rgb_B))/3;
			          		rgb_th_WK_Image.setRGB(i, j, (bb));
			          		if(largest_WK < bb)
			          		{
			          			largest_WK = bb;
			          		}
			          		WK_part = (rgb_R) + (rgb_G) + (rgb_B);
				          	if( get_WK_cnt[fish_no] < WK_part )  get_WK_cnt[fish_no] = WK_part ;
				          	if( K_get_min[fish_no] > WK_part )  K_get_min[fish_no] = WK_part ;
				          	get_WK_area[fish_no][WK_part]++;
				        }
			          	else{
			          		rgb_th_WK_Image.setRGB(i, j, 0);
			        	}
			        }
		        }
	    		export(img2Mat(rgb_th_WK_Image), "rgb_th_WK_Image_"+fish_no);
//System.out.println("before LTP_math, Using Time = "+ (System.currentTimeMillis()-startTime)+" ms");

Mat BChannels = new Mat(mat.size(),mat.type());
Mat GChannels = new Mat(mat.size(),mat.type());
Mat RChannels = new Mat(mat.size(),mat.type());
BChannels = BGRChannels.get(0).clone();
GChannels = BGRChannels.get(1).clone();
RChannels = BGRChannels.get(2).clone();
Imgproc.cvtColor(BChannels,BChannels,Imgproc.COLOR_GRAY2RGB);
Imgproc.cvtColor(GChannels,GChannels,Imgproc.COLOR_GRAY2RGB);
Imgproc.cvtColor(RChannels,RChannels,Imgproc.COLOR_GRAY2RGB);

//List<int[]> LTP_PM_R_result = new ArrayList<int[]>();
//List<int[]> LTP_PM_G_result = new ArrayList<int[]>();
//List<int[]> LTP_PM_B_result = new ArrayList<int[]>();
List<int[]> LTP_PM_WKarea_result = new ArrayList<int[]>();
List<int[]> LTP_PM_Rarea_result = new ArrayList<int[]>();
//List<int[]> WK_QR_result = new ArrayList<int[]>();
//Mat mat_SML = new Mat();
//Mat mat_segRed = new Mat();
//List<Mat> segRed_result = new ArrayList<Mat>();
List<Mat> mat_SML = new ArrayList<Mat>();

//System.out.println("mat_SML");
LTP_PM_WKarea_result= LTP_math(img2Mat(rgb_th_WK_Image),fish_no,stream_no,contour_Rarea);
LTP_PM_Rarea_result= LTP_math(img2Mat(hsv_r_get_Image),fish_no,stream_no,contour_WKarea);
mat_SML = WK_QR(mat,fish_no,stream_no,0,150+Yth,Yth);//Yth = 0~9
//else{WK_QR_result = WK_QR(mat,fish_no,stream_no,contour_B,65+Yth,Yth);}
//System.out.println("there");
P_hist_reg[contour_Rarea][fish_no] = LTP_PM_Rarea_result.get(0).clone();
P_hist_reg[contour_WKarea][fish_no] = LTP_PM_WKarea_result.get(0).clone();
//P_hist_reg[contour_R][fish_no] = WK_QR_result.get(0).clone();
//P_hist_reg[contour_WKarea][fish_no][stream_no] = LTP_PM_WKarea_result.get(0).clone();





//System.out.println("mat_SML");
//TODO
//--------------------  get Koi kind begin  ----------------------
				if (K_get_min[fish_no] >= 110 && get_R_cnt[fish_no] >= 70)
					{koi_kind[fish_no] = kind_Kohaku;}
				else if (K_get_min[fish_no] < 110 && get_R_cnt[fish_no] >= 70)
					{koi_kind[fish_no] = kind_Showa_Sanshoku;}
				else if (K_get_min[fish_no] < 110 && get_R_cnt[fish_no] < 70)
					{koi_kind[fish_no] = kind_Bekko;}
				else
					{koi_kind[fish_no] = kind_others;}
//--------------------  get Koi kind end  ----------------------		        
				System.out.println("K_get_min[fish_no] = "+K_get_min[fish_no] +" ,get_R_cnt[fish_no] = "+get_R_cnt[fish_no]);
				//System.out.println("Process "+(fish_no+1)+" End ! Using time "+ (System.currentTimeMillis()-startTime)+" ms");	        	
				JSONObject Cut_param = new JSONObject();
				JSONArray Cut_sd1 = new JSONArray();
				JSONArray Cut_sc1 = new JSONArray();
				JSONArray Cut_sc2 = new JSONArray();
				JSONArray Cut_sc3 = new JSONArray();
				JSONArray Cut_vlc1 = new JSONArray();
				JSONArray Cut_vlc2 = new JSONArray();
				VLC vlc = new VLC();
				String[] vlc_str_R = new String[254];
				String[] vlc_str_W = new String[254];
////		    	Mat vlc_mat = new Mat();
				export(mat_SML.get(0), "mat_SML_R_"+stream_no+"_"+fish_no);
				export(mat_SML.get(1), "mat_SML_W_"+stream_no+"_"+fish_no);
				vlc.VLC_Decoder(mat_SML.get(0), vlc_str_R);
				vlc.VLC_Decoder(mat_SML.get(1), vlc_str_W);
				
				
				
				
				//for(int i = 0 ; i < koi_size ; i ++){
				if (get_R_cnt[0]/8 > 0 ){
				    for(int i = 0 ; i < koi_size ; i ++){
				    	for(int j = 0 ; j < get_R_cnt[i] ; j ++){
				    		if(j >= 0 &j < get_R_cnt[i]/8)
				    		{avg_hist_area[i][0] = avg_hist_area[i][0] + get_R_area[i][j];}
				    		else if(j >= get_R_cnt[i]/8 &j < (get_R_cnt[i]*2)/8)
				    		{avg_hist_area[i][1] = avg_hist_area[i][1] + get_R_area[i][j];}
				    		else if(j >= (get_R_cnt[i]*2)/8 &j < (get_R_cnt[i]*3)/8)
				    		{avg_hist_area[i][2] = avg_hist_area[i][2] + get_R_area[i][j];}
				    		else if(j >= (get_R_cnt[i]*3)/8 &j < (get_R_cnt[i]*4)/8)
				    		{avg_hist_area[i][3] = avg_hist_area[i][3] + get_R_area[i][j];}
				    		else if(j >= (get_R_cnt[i]*4)/8 &j < (get_R_cnt[i]*5)/8)
				    		{avg_hist_area[i][4] = avg_hist_area[i][4] + get_R_area[i][j];}
				    		else if(j >= (get_R_cnt[i]*5)/8 &j < (get_R_cnt[i]*6)/8)
				    		{avg_hist_area[i][5] = avg_hist_area[i][5] + get_R_area[i][j];}
				    		else if(j >= (get_R_cnt[i]*6)/8 &j < (get_R_cnt[i]*7)/8)
				    		{avg_hist_area[i][6] = avg_hist_area[i][6] + get_R_area[i][j];}
				    		else if(j >= (get_R_cnt[i]*7)/8 &j < get_R_cnt[i])
				    		{avg_hist_area[i][7] = avg_hist_area[i][7] + get_R_area[i][j];}
				    	}
				    }
				}
				else{
					for(int i = 0 ; i < koi_size ; i ++){
						for(int j = 0 ; j < 8 ; j ++){avg_hist_area[i][j] = 0;}
					}
				}
				
				for(int j = 0 ; j < 8 ; j ++){	
					if (get_R_cnt[fish_no]/8 > 0 &&rgb_th_erodil_Image_area[fish_no]  > 0 ){
						Cut_sd1.put(j, (int) ((long)(avg_hist_area[fish_no][j]*10000 )/(long)((get_R_cnt[fish_no]/8)* rgb_th_erodil_Image_area[fish_no])));
				//			avg_hist_area_result[i][j] = (int) ((long)(avg_hist_area[i][j]*10000 )/(long)((get_R_cnt[i]/8)* rgb_th_erodil_Image_area[i]));
					}
					else{
						Cut_sd1.put(j,0);
				//			avg_hist_area_result[i][j] = 0;
					}
				}
				for(int j = 1 ; j < 255 ; j++ ){
					Cut_sc1.put(j-1, P_hist_reg[contour_Rarea][fish_no][j]);
				//		P_hist_reg_Rarea_result[i][j] = P_hist_reg[contour_Rarea][i][0][j];
				}
				for(int j = 1 ; j < 255 ; j++ ){
					Cut_sc2.put(j-1, P_hist_reg[contour_WKarea][fish_no][j]);
				//		P_hist_reg_WKarea_result[i][j] = P_hist_reg[contour_WKarea][i][0][j];
				}
				for(int j = 1 ; j < 255 ; j++ ){
					Cut_sc3.put(j-1, P_hist_reg[contour_R][fish_no][j]);
				//		P_hist_reg_R_result[i][j] = P_hist_reg[contour_R][i][0][j];
				}
				for(int j = 0 ; j < 254 ; j++ ){
					
					Cut_vlc1.put(j, vlc_str_R[j]);
				}
				for(int j = 0 ; j < 254 ; j++ ){
					
					Cut_vlc2.put(j, vlc_str_W[j]);
				}
				//for(int j = 1 ; j < 255 ; j++ ){
				//	P_hist_reg_B_result[i][j] = P_hist_reg[contour_B][i][stream_no][j];
				//}
				//}
				Cut_param.put("sd1",Cut_sd1);
				Cut_param.put("sc1",Cut_sc1);
				Cut_param.put("sc2",Cut_sc2);
				Cut_param.put("sc3",Cut_sc3);
				Cut_param.put("vlc1",Cut_vlc1);
				Cut_param.put("vlc2",Cut_vlc2);
				Cut_N.put("param", Cut_param);
//				NewDataKoi.put(Cut_no, Cut_N);
				JSON_string = JSON_string + Cut_N + "\r\n" ;
				}
	        
	        
        
	        // ----------- write image
	        for(int i = 0 ; i < koi_size ; i ++){
	        	Mat wei_output_mat = new Mat(400, 100, Kois.get(i+koi_size).type());
	        	Imgproc.resize(Kois.get(i+koi_size), wei_output_mat, wei_output_mat.size() , 0, 0, Imgproc.INTER_LINEAR);
	        	ByteArrayOutputStream Image_out = new ByteArrayOutputStream();
	        	ImageIO.write(mat2Img(Kois.get(i+koi_size)), "bmp", Image_out);
	        	byte[] Image_out_resizedBytes = Image_out.toByteArray();
	        	
	        	File Image_out_resizedImgFile = new File(output_path+"Cut_"+(i+1)+".bmp");
	        	
	        	if(!Image_out_resizedImgFile.exists())
	        	{
	        		Image_out_resizedImgFile.getParentFile().mkdirs();
	        	    
	        	    try {
	        	    	FileOutputStream Image_out_fos = new FileOutputStream(Image_out_resizedImgFile);
			        	Image_out_fos.write(Image_out_resizedBytes);
	        	    	Image_out_resizedImgFile.createNewFile();
	    	        	Image_out_fos.close();
	        	    } catch (IOException e) {
	        	        // TODO Auto-generated catch block
	        	        e.printStackTrace();
	        	    }
	        	}
	        }
	        // ----------- show kind
	    	for (int i = 0; i < koi_size; i++) {
	                System.out.println("No."+ (i + 1) +" koi is [ " + fishId[koi_kind[i]] +" "+fishId_name[koi_kind[i]]+" ] ");
	        }

		    
//		    System.out.println("txt_cnt = " + (long)(txt_cnt/koi_size));
		    int[][] koi_size_result = new int[1][20] ;
		    koi_size_result[0][0] = koi_size;
		    Koi_process_result.add(avg_hist_area_result);
		    Koi_process_result.add(P_hist_reg_Rarea_result);
		    Koi_process_result.add(P_hist_reg_WKarea_result);
		    Koi_process_result.add(P_hist_reg_R_result);
		    Koi_process_result.add(P_hist_reg_G_result);
		    Koi_process_result.add(P_hist_reg_B_result);
//		    Koi_process_result.add(koi_size_result);
		    Koi_process_result.add(koi_size_result);
		    for (int i = 0; i < koi_size; i++) {
			    koi_size_result[0][i] = fishId[koi_kind[i]];
		    }
		    Koi_process_result.add(koi_size_result);
		    
	    }
	    else{
//	    	int[][] koi_size_result = new int[1][koi_size] ;
//		    koi_size_result[0][0] = koi_size;
//		    
//	    	Koi_process_result.add(koi_size_result);
//		    Koi_process_result.add(koi_size_result);
//		    Koi_process_result.add(koi_size_result);
//		    Koi_process_result.add(koi_size_result);
//		    Koi_process_result.add(koi_size_result);
//		    Koi_process_result.add(koi_size_result);
//		    Koi_process_result.add(koi_size_result);
//		    for (int i = 0; i < koi_size; i++) {
//			    koi_size_result[0][i] = fishId[koi_kind[i]];
//		    }
//		    Koi_process_result.add(koi_size_result)
	    }
//        ----------------end
		return JSON_string;//Koi_process_result;//Cut_N;
        } catch (Exception e) {
		    // TODO: handle exception
        	System.out.println(e);
        	return null;
		}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static List<int[]> LTP_math(Mat mat,int fish_no,int stream,int type)
    // 做LTP演算法，然後輸出LTP藍圖跟histogram
    // mat = input image
    // fish_no = 哪一隻魚(魚編號)
    // stream = 目前的串流編號
    // type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
    {
    	int[][][][] P_hist;
    	int[][][][] M_hist;
    	P_hist = new int[10][20][1000][256];
    	M_hist = new int[10][20][1000][256];
//    	byte[] data = {0,0,0};
//    	byte[] data_Black = {0,0,0};
//    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
//    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
//    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	byte[][] data_LTP;
    	byte[] center_LTP;
    	int data_LTP_P_total;
    	int data_LTP_M_total;
//    	List<Mat> LTP_Kois = new ArrayList<Mat>();
    	List<int[]> LTP_PM_result = new ArrayList<int[]>();
//    	Mat LTP_P = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
//    	Mat LTP_M = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	
//    	export(mat, "mat_s"+stream);
    	
    	data_LTP = new byte[8][3];
    	center_LTP = new byte[3];
    	data_LTP_P_total = 0;
    	data_LTP_M_total = 0;
//    	int[] center_LTP ;
//    	center_LTP = new int[8]; 	
    	int all_area = 0;
    	all_area = mat.width() * mat.height();
//    	int div_area = 0;
//    	div_area = all_area;
    	int mix_value = 100000;
//    	while(div_area/10 >0){
//    		div_area = div_area / 10;
//    		mix_value = mix_value * 10;
//    	}
//    	
    	for(int i = 1 ; i < mat.width() -1 ; i ++){
    		for(int j = 1 ; j < mat.height() -1 ; j ++){
    			mat.get(j-1, i-1, data_LTP[3]);
    			mat.get(j  , i-1, data_LTP[2]);
    			mat.get(j+1, i-1, data_LTP[1]);
    			mat.get(j-1, i  , data_LTP[4]);
    			mat.get(j+1, i  , data_LTP[0]);
    			mat.get(j-1, i+1, data_LTP[5]);
    			mat.get(j  , i+1, data_LTP[6]);
    			mat.get(j+1, i+1, data_LTP[7]);
    			mat.get(j  , i  , center_LTP);
    			
    			for(int k = 0 ; k < 8 ; k ++){
//    				if(data_LTP[k][0] < 0){
//        				System.out.println("data_LTP = "+data_LTP[k][0] + " at ( " + i +" , "+ j +" )");
//        			}
	    			if(center_LTP[0] - data_LTP[k][0] >= 0){
	    				data_LTP_P_total = data_LTP_P_total + (int)Math.pow(2 , k);
//	    				for(int l = 0; l < 32 ; l ++)P_hist[type][fish_no][stream][k*32+l]++;
//	    				P_hist[type][fish_no][stream][k]++;
	    			}
	    			if(center_LTP[0] - data_LTP[k][0] < 3){
//	    				M_hist[type][fish_no][stream][k]++;
	    				data_LTP_M_total = data_LTP_M_total + (int)Math.pow(2 , k);
	    			}
//	    			else{
//	    				
//	    			}
    			}
    			P_hist[type][fish_no][stream][data_LTP_P_total]++;
    			M_hist[type][fish_no][stream][data_LTP_M_total]++;
//    			byte[] data_P_out = {0,0,(byte) ((data_LTP_P_total))};
//    			byte[] data_M_out = {0,0,(byte) ((data_LTP_M_total))};
//    			LTP_P.put(j, i, data_P_out);
//    			LTP_M.put(j, i, data_M_out);
    			data_LTP_P_total = 0;
    			data_LTP_M_total = 0;
        	}
    	}
    	int compare_P = 0;
    	int compare_M = 0;
    	for(int i=1  ; i < 255 ; i++){
//    		System.out.println( P_hist[i]);
//    		P_hist[i] = P_hist[i] / 100;
//    		M_hist[i] = M_hist[i] / 100;
    		if(P_hist[type][fish_no][stream][i]>compare_P){compare_P = P_hist[type][fish_no][stream][i];}//(int)P_hist[stream][i];}
    		if(M_hist[type][fish_no][stream][i]>compare_M){compare_M = M_hist[type][fish_no][stream][i];}//(int)M_hist[stream][i];}
    	}
//    	Mat LTP_P_hist = new Mat(compare_P, 256, CvType.CV_8UC3);
//    	Mat LTP_M_hist = new Mat(compare_M, 256, CvType.CV_8UC3);
    	
//		for(int j=1 ; j < 255 ; j++){
//			for(int i=0 ; i < compare_P ; i++){
//    			if(P_hist[type][fish_no][stream][j]<i){LTP_P_hist.put(compare_P-i, j, data_White);}
//    			else{LTP_P_hist.put(compare_P-i, j, data_Black);}
//    		}
//    	}
//		for(int j=1 ; j < 255 ; j++){
//			for(int i=1 ; i < compare_M ; i++){
//				if(M_hist[type][fish_no][stream][j]<i){LTP_M_hist.put(compare_M-i, j, data_White);}
//				else{LTP_M_hist.put(compare_M-i, j, data_Black);}
//			}
//    	}
		for(int i=1  ; i < 255 ; i++){
			P_hist[type][fish_no][stream][i] = (P_hist[type][fish_no][stream][i]*mix_value)/all_area;
			M_hist[type][fish_no][stream][i] = (M_hist[type][fish_no][stream][i]*mix_value)/all_area;
		}
		
		LTP_PM_result.add(P_hist[type][fish_no][stream]);
		LTP_PM_result.add(M_hist[type][fish_no][stream]);
//    	LTP_Kois.add(LTP_P);
//    	LTP_Kois.add(LTP_M);
//    	LTP_Kois.add(LTP_P_hist);
//    	LTP_Kois.add(LTP_M_hist);
//    	export(LTP_P, "LTP_P_s"+fish_no+stream);
//    	export(LTP_M, "LTP_P_s"+fish_no+stream);
//    	export(LTP_P_hist, "LTP_P_hist_s"+fish_no+stream);
//    	export(LTP_M_hist, "LTP_P_hist_s"+fish_no+stream);
//    	System.out.println(LTP_Kois);
    	return LTP_PM_result;//return LTP_Kois;
    }
    public static List<int[]> LTP_8centers(Mat mat,int fish_no,int stream,int type)
    // 做LTP演算法取八方向中心，然後輸出LTP藍圖與中心坐標跟histogram
    // mat = input image
    // fish_no = 哪一隻魚(魚編號)
    // stream = 目前的串流編號
    // type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
    {
    	int[][][][] P_hist;
    	int[][][][] M_hist;
    	P_hist = new int[10][20][1000][256];
    	M_hist = new int[10][20][1000][256];
//    	byte[] data = {0,0,0};
    	byte[] data_Black = {0,0,0};
    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
//    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	byte[][] data_LTP;
    	byte[] center_LTP;
//    	int data_LTP_P_total;
//    	int data_LTP_M_total;
    	
    	for(int k = 0 ; k < 8 ; k ++){P_hist[type][fish_no][stream][k]=0;}
    	// 正向LTP初始化歸0
    	
    	List<Mat> LTP_Kois = new ArrayList<Mat>();
    	List<int[]> LTP_PM_result = new ArrayList<int[]>();
    	Mat LTP_P = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	Mat LTP_M = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	
    	export(mat, "mat_s"+stream+"_"+fish_no+"_"+type);
    	
    	data_LTP = new byte[8][3];
    	center_LTP = new byte[3];
//    	data_LTP_P_total = 0;
//    	data_LTP_M_total = 0;
//    	int[] center_LTP ;
//    	center_LTP = new int[8]; 	
//    	int all_area = 0;
//    	all_area = mat.width() * mat.height();
//    	int div_area = 0;
//    	div_area = all_area;
//    	int mix_value = 100000;
//    	while(div_area/10 >0){
//    		div_area = div_area / 10;
//    		mix_value = mix_value * 10;
//    	}
//    	
    	long[] center_x_cnt = new long[8];
    	long[] center_y_cnt = new long[8];
    	long[] center_x_total = new long[8];
    	long[] center_y_total = new long[8];
    	for(int k = 0 ; k < 8 ; k ++){
    		center_x_cnt[k]=0;center_y_cnt[k]=0;center_x_total[k]=0;center_y_total[k]=0;
    	}
    	for(int i = 1 ; i < mat.width() -1 ; i ++){
    		for(int j = 1 ; j < mat.height() -1 ; j ++){
    			mat.get(j-1, i-1, data_LTP[3]);
    			mat.get(j-1, i  , data_LTP[2]);
    			mat.get(j-1, i+1, data_LTP[1]);
    			mat.get(j  , i-1, data_LTP[4]);
    			mat.get(j  , i+1, data_LTP[0]);
    			mat.get(j+1, i-1, data_LTP[5]);
    			mat.get(j+1, i  , data_LTP[6]);
    			mat.get(j+1, i+1, data_LTP[7]);
    			mat.get(j  , i  , center_LTP);
    			//     -----------
    			//    | 3 | 2 | 1 | j-1
    			//    |---|---|---|
    			//    | 4 | C | 0 | j    C = center
    			//    |---|---|---|
    			//    | 5 | 6 | 7 | j+1
    			//     -----------
    			//     i-1  i  i+1
    			
    			for(int k = 0 ; k < 8 ; k ++){
//    				if(data_LTP[k][0] < 0){
//        				System.out.println("data_LTP = "+data_LTP[k][0] + " at ( " + i +" , "+ j +" )");
//        			}
    				
	    			if(center_LTP[0] - data_LTP[k][0] > 5){
	    			// LTP公式，中心點減掉周圍大於5，該方向的正向量LTP成立
	    				
//	    				data_LTP_P_total = data_LTP_P_total + (int)Math.pow(2 , k);
//	    				for(int l = 0; l < 32 ; l ++)P_hist[type][fish_no][stream][k*32+l]++;

	    				P_hist[type][fish_no][stream][k]++;
	    				// 累加直方圖
	    				
	    				LTP_P.put(j, i, data_Gary);
	    				// 為了方便觀察，將LTP成像存成影像時用白色跟灰色存

	    				/*累加數量與座標值，之後用於計算中心點位置*/
	    				center_x_total[k] = center_x_total[k] + i ;
	    				center_x_cnt[k]++;
	    				center_y_total[k] = center_y_total[k] + j ;
	    				center_y_cnt[k]++;
	    				/*累加數量與座標值，之後用於計算中心點位置*/ 
	    				
	    			}else LTP_P.put(j, i, data_White);
//	    			if(center_LTP[0] - data_LTP[k][0] < 3){
//	    				M_hist[type][fish_no][stream][k]++;
//	    				center_x_total[k] = center_x_total[k] + i ;
//	    				center_x_cnt[k]++;
//	    				center_y_total[k] = center_y_total[k] + j ;
//	    				center_y_cnt[k]++;
////	    				data_LTP_M_total = data_LTP_M_total + (int)Math.pow(2 , k);
//	    			}
//	    			else{
//	    				
//	    			}
    			}
//    			P_hist[type][fish_no][stream][data_LTP_P_total]++;
//    			M_hist[type][fish_no][stream][data_LTP_M_total]++;
//    			byte[] data_P_out = {0,0,(byte) ((data_LTP_P_total))};
//    			byte[] data_M_out = {0,0,(byte) ((data_LTP_M_total))};
//    			LTP_P.put(j, i, data_P_out);
//    			LTP_M.put(j, i, data_M_out);
//    			LTP_P.put(j, i, data_White);
//    			LTP_M.put(j, i, data_White);
//    			data_LTP_P_total = 0;
//    			data_LTP_M_total = 0;
        	}
    	}
    	int compare_P = 0;
    	int compare_M = 0;
    	for(int i=1  ; i < 255 ; i++){
//    		System.out.println( P_hist[type][fish_no][stream][i]);
//    		P_hist[i] = P_hist[i] / 100;
//    		M_hist[i] = M_hist[i] / 100;
    		
    		if(P_hist[type][fish_no][stream][i]>compare_P){compare_P = P_hist[type][fish_no][stream][i];}//(int)P_hist[stream][i];}
    		if(M_hist[type][fish_no][stream][i]>compare_M){compare_M = M_hist[type][fish_no][stream][i];}//(int)M_hist[stream][i];}
    		// 取得正向LTP最大值跟負向LTP最大值，用於等一下宣告圖片的範圍
    	}
    	Mat LTP_P_hist = new Mat(compare_P, 256, CvType.CV_8UC3);
    	Mat LTP_M_hist = new Mat(compare_M, 256, CvType.CV_8UC3);
    	// 將 LTP histogram 存成圖片，範圍是 256 * histogram最大值
    	
		for(int j=1 ; j < 255 ; j++){
			for(int i=0 ; i < compare_P ; i++){
    			if(P_hist[type][fish_no][stream][j]<i){LTP_P_hist.put(compare_P-i, j, data_White);}
    			else{LTP_P_hist.put(compare_P-i, j, data_Black);}
    		}
    	}// 正LTP成像存成圖片
		
		for(int j=1 ; j < 255 ; j++){
			for(int i=1 ; i < compare_M ; i++){
				if(M_hist[type][fish_no][stream][j]<i){LTP_M_hist.put(compare_M-i, j, data_White);}
				else{LTP_M_hist.put(compare_M-i, j, data_Black);}
			}
    	}// 負LTP成像存成圖片
		
//		for(int i=1  ; i < 255 ; i++){
//			P_hist[type][fish_no][stream][i] = (P_hist[type][fish_no][stream][i]);//*mix_value)/all_area;
//			M_hist[type][fish_no][stream][i] = (M_hist[type][fish_no][stream][i]);//*mix_value)/all_area;
//		}
		
		LTP_PM_result.add(P_hist[type][fish_no][stream]);
		LTP_PM_result.add(M_hist[type][fish_no][stream]);
		// 將LTP正負的累加值INT陣列存入回傳list中
    	LTP_Kois.add(LTP_P);
    	LTP_Kois.add(LTP_M);
    	LTP_Kois.add(LTP_P_hist);
    	LTP_Kois.add(LTP_M_hist);
    	// 將LTP正負的累加值跟直方圖存入回傳list中
    	
    	/*計算並畫出LTP正的8個方向向量分布的中心點*/
    	for(int k = 0 ; k < 8 ; k ++){
    		Imgproc.circle(LTP_P,
	    	new Point(center_x_total[k]/center_x_cnt[k], center_y_total[k]/center_y_cnt[k]), 
	    	1, 
	    	new Scalar((32*k), (255-32*k), 255),2);
	    	export(LTP_P, "LTP_P_s"+fish_no+stream+type);
	    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
	    }
    	/*計算並畫出LTP正的8個方向向量分布的中心點*/
    	
//    	export(LTP_M, "LTP_M_s"+fish_no+stream+type);
//    	export(LTP_P_hist, "LTP_P_hist_s"+fish_no+stream+type);
//    	export(LTP_M_hist, "LTP_M_hist_s"+fish_no+stream+type);
//    	System.out.println(LTP_Kois);
    	System.out.println("center_x_total = ("+center_x_total[0]+") , center_x_cnt = ("+center_x_cnt[0]+")");
    	System.out.println("center_y_total = ("+center_y_total[0]+") , center_y_cnt = ("+center_y_cnt[0]+")");
    	
    	
//    	System.out.println("center(x5,y5) = ("+center_x_total[5]/center_x_cnt[5]+" , "+center_y_total[5]/center_y_cnt[5]+")");
    	return LTP_PM_result;//return LTP_Kois;
    }
    
    public static List<int[]> LTP_256centers(Mat mat,int fish_no,int stream,int type)
    // 做LTP演算法取256專屬向量編碼，然後輸出LTP藍圖與256個中心坐標跟histogram
    // mat = input image
    // fish_no = 哪一隻魚(魚編號)
    // stream = 目前的串流編號
    // type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
    {
    	int[][][][] P_hist;
    	int[][][][] M_hist;
    	P_hist = new int[10][20][1000][256];
    	M_hist = new int[10][20][1000][256];
//    	byte[] data = {0,0,0};
    	byte[] data_Black = {0,0,0};
//    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
//    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	byte[][] data_LTP;
    	byte[] center_LTP;
    	int data_LTP_P_total;
    	int data_LTP_M_total;
    	for(int k = 0 ; k < 8 ; k ++){P_hist[type][fish_no][stream][k]=0;}
    	List<Mat> LTP_Kois = new ArrayList<Mat>();
    	List<int[]> LTP_PM_result = new ArrayList<int[]>();
    	Mat LTP_P_0 = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	Mat LTP_M = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	
    	export(mat, "mat_s"+stream+"_"+fish_no+"_"+type);
    	
    	data_LTP = new byte[8][3];
    	center_LTP = new byte[3];
    	data_LTP_P_total = 0;
    	data_LTP_M_total = 0;
    	long[] center_x_cnt = new long[256];
    	long[] center_y_cnt = new long[256];
    	long[] center_x_total = new long[256];
    	long[] center_y_total = new long[256];
    	for(int k = 0 ; k < 256 ; k ++){
    		center_x_cnt[k]=0;center_y_cnt[k]=0;center_x_total[k]=0;center_y_total[k]=0;
    	}
    	for(int i = 1 ; i < mat.width() -1 ; i ++){
    		for(int j = 1 ; j < mat.height() -1 ; j ++){
    			mat.get(j-1, i-1, data_LTP[3]);
    			mat.get(j  , i-1, data_LTP[2]);
    			mat.get(j+1, i-1, data_LTP[1]);
    			mat.get(j-1, i  , data_LTP[4]);
    			mat.get(j+1, i  , data_LTP[0]);
    			mat.get(j-1, i+1, data_LTP[5]);
    			mat.get(j  , i+1, data_LTP[6]);
    			mat.get(j+1, i+1, data_LTP[7]);
    			mat.get(j  , i  , center_LTP);
    			
    			for(int k = 0 ; k < 8 ; k ++){
	    			if(center_LTP[0] - data_LTP[k][0] > 5){
	    				data_LTP_P_total = data_LTP_P_total + (int)Math.pow(2 , k);
	    			}
	    			if(center_LTP[0] - data_LTP[k][0] < 3){
	    				data_LTP_M_total = data_LTP_M_total + (int)Math.pow(2 , k);
	    			}
    			}
				center_x_total[data_LTP_P_total] = center_x_total[data_LTP_P_total] + i ;
				center_x_cnt[data_LTP_P_total]++;
				center_y_total[data_LTP_P_total] = center_y_total[data_LTP_P_total] + j ;
				center_y_cnt[data_LTP_P_total]++;
    			data_LTP_P_total = 0;
    			data_LTP_M_total = 0;
        	}
    	}
    	int compare_P = 0;
    	int compare_M = 0;
    	for(int i=1  ; i < 255 ; i++){
    		if(P_hist[type][fish_no][stream][i]>compare_P){compare_P = P_hist[type][fish_no][stream][i];}//(int)P_hist[stream][i];}
    		if(M_hist[type][fish_no][stream][i]>compare_M){compare_M = M_hist[type][fish_no][stream][i];}//(int)M_hist[stream][i];}
    	}
    	Mat LTP_P_hist = new Mat(compare_P, 256, CvType.CV_8UC3);
    	Mat LTP_M_hist = new Mat(compare_M, 256, CvType.CV_8UC3);
    	
		for(int j=1 ; j < 255 ; j++){
			for(int i=0 ; i < compare_P ; i++){
    			if(P_hist[type][fish_no][stream][j]<i){LTP_P_hist.put(compare_P-i, j, data_White);}
    			else{LTP_P_hist.put(compare_P-i, j, data_Black);}
    		}
    	}
		for(int j=1 ; j < 255 ; j++){
			for(int i=1 ; i < compare_M ; i++){
				if(M_hist[type][fish_no][stream][j]<i){LTP_M_hist.put(compare_M-i, j, data_White);}
				else{LTP_M_hist.put(compare_M-i, j, data_Black);}
			}
    	}
		LTP_PM_result.add(P_hist[type][fish_no][stream]);
		LTP_PM_result.add(M_hist[type][fish_no][stream]);
    	LTP_Kois.add(LTP_P_0);
    	LTP_Kois.add(LTP_M);
    	LTP_Kois.add(LTP_P_hist);
    	LTP_Kois.add(LTP_M_hist);
    	LTP_P_0 = mat.clone();
    	for(int k = 0 ; k < 256 ; k ++){
    		if(center_x_cnt[k] > 0 && center_y_cnt[k] > 0){
	    		Imgproc.circle(LTP_P_0,
		    	new Point(center_x_total[k]/center_x_cnt[k], center_y_total[k]/center_y_cnt[k]), 
		    	1, 
		    	new Scalar((k), (255-k), 255),2);
		    	export(LTP_P_0, "LTP_P_s"+fish_no+stream+type);
		    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
    		}
	    }
    	System.out.println("center_x_total = ("+center_x_total[0]+") , center_x_cnt = ("+center_x_cnt[0]+")");
    	System.out.println("center_y_total = ("+center_y_total[0]+") , center_y_cnt = ("+center_y_cnt[0]+")");
    	return LTP_PM_result;//return LTP_Kois;
    }
    public static List<int[]> High_Line(Mat mat, int fish_no, int stream, int type, int max_value)
        // 做LTP演算法取256專屬向量編碼，然後輸出LTP藍圖與256個中心坐標跟histogram
    // mat = input image
    // fish_no = 哪一隻魚(魚編號)
    // stream = 目前的串流編號
    // type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
    {
    	int[][][][] P_hist;
    	int[][][][] M_hist;
    	P_hist = new int[10][20][1000][256];
    	M_hist = new int[10][20][1000][256];
//    	byte[] data = {0,0,0};
    	byte[] data_Black = {0,0,0};
//    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
//    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	byte[][] data_LTP;
    	byte[] center_LTP;
    	int data_LTP_P_total;
    	int data_LTP_M_total;
    	Mat dest = new Mat(mat.size(), mat.type());
    	Mat dest_clean = new Mat(mat.size(), mat.type(),new Scalar(0, 0, 0));
    	for(int k = 0 ; k < 8 ; k ++){P_hist[type][fish_no][stream][k]=0;}
    	List<Mat> LTP_Kois = new ArrayList<Mat>();
    	List<int[]> LTP_PM_result = new ArrayList<int[]>();
    	Mat LTP_P_0 = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	Mat LTP_M = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	
    	export(mat, "mat_s"+stream+"_"+fish_no+"_"+type);
    	
    	data_LTP = new byte[8][3];
    	center_LTP = new byte[3];
    	data_LTP_P_total = 0;
    	data_LTP_M_total = 0;
    	long[] center_x_cnt = new long[256];
    	long[] center_y_cnt = new long[256];
    	long[] center_x_total = new long[256];
    	long[] center_y_total = new long[256];
    	for(int k = 0 ; k < 256 ; k ++){
    		center_x_cnt[k]=0;center_y_cnt[k]=0;center_x_total[k]=0;center_y_total[k]=0;
    	}
    	for(int i = 1 ; i < mat.width() -1 ; i ++){
    		for(int j = 1 ; j < mat.height() -1 ; j ++){
    			mat.get(j  , i  , center_LTP);
    			
    			for(int k = 0 ; k < 8 ; k ++){
	    			if(center_LTP[0] - data_LTP[k][0] > 5){
	    				data_LTP_P_total = data_LTP_P_total + (int)Math.pow(2 , k);
	    			}
	    			if(center_LTP[0] - data_LTP[k][0] < 3){
	    				data_LTP_M_total = data_LTP_M_total + (int)Math.pow(2 , k);
	    			}
    			}
        	}
    	}
    	int compare_P = 0;
    	int compare_M = 0;
    	for(int i=1  ; i < 255 ; i++){
    		if(P_hist[type][fish_no][stream][i]>compare_P){compare_P = P_hist[type][fish_no][stream][i];}//(int)P_hist[stream][i];}
    		if(M_hist[type][fish_no][stream][i]>compare_M){compare_M = M_hist[type][fish_no][stream][i];}//(int)M_hist[stream][i];}
    	}
    	Mat LTP_P_hist = new Mat(compare_P, 256, CvType.CV_8UC3);
    	Mat LTP_M_hist = new Mat(compare_M, 256, CvType.CV_8UC3);
    	
		for(int j=1 ; j < 255 ; j++){
			for(int i=0 ; i < compare_P ; i++){
    			if(P_hist[type][fish_no][stream][j]<i){LTP_P_hist.put(compare_P-i, j, data_White);}
    			else{LTP_P_hist.put(compare_P-i, j, data_Black);}
    		}
    	}
		for(int j=1 ; j < 255 ; j++){
			for(int i=1 ; i < compare_M ; i++){
				if(M_hist[type][fish_no][stream][j]<i){LTP_M_hist.put(compare_M-i, j, data_White);}
				else{LTP_M_hist.put(compare_M-i, j, data_Black);}
			}
    	}
		LTP_PM_result.add(P_hist[type][fish_no][stream]);
		LTP_PM_result.add(M_hist[type][fish_no][stream]);
    	LTP_Kois.add(LTP_P_0);
    	LTP_Kois.add(LTP_M);
    	LTP_Kois.add(LTP_P_hist);
    	LTP_Kois.add(LTP_M_hist);
    	LTP_P_0 = mat.clone();
    	ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
    	List<ArrayList<MatOfPoint>> array_contours = new ArrayList<ArrayList<MatOfPoint>>();
    	Mat drawCon = new Mat();
//    	Mat drawCon_mat = new Mat();
    	if(max_value>8){
	    	int contour_step = 0; 
	    	contour_step = max_value / 8 ;
	    	System.out.println("max_value"+type+" = "+max_value);
			System.out.println("contour_step"+type+" = "+contour_step);
			for(int i = 0 ; i < max_value ; i = i + contour_step){
				Core.inRange(mat, new Scalar(i, 0, 0), new Scalar(255, 255, 255), dest);
				Imgproc.findContours(dest,
				contours,
				drawCon, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				array_contours.add(contours) ;
			}
	//		export(dest, "a_dest");
			dest = mat.clone();
			export(dest, "a_dest");
			for(int i = 0; i < 8 ; i++){
				for (int k = 0; k < array_contours.get(i).size(); k++) {
					Imgproc.drawContours(dest, array_contours.get(i), k, new Scalar(0, 255, 255), 1);
					Imgproc.drawContours(dest_clean, array_contours.get(i), k, new Scalar(150, 255, 255), 1);
				}
			}
			export(dest, "a_destcontours"+fish_no+type);
			export(dest_clean, "dest_clean_contours"+fish_no+type);
    	}
    	for(int k = 0 ; k < 256 ; k ++){
    		if(center_x_cnt[k] > 0 && center_y_cnt[k] > 0){
    			
		    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
    		}
	    }
    	System.out.println("center_x_total = ("+center_x_total[0]+") , center_x_cnt = ("+center_x_cnt[0]+")");
    	System.out.println("center_y_total = ("+center_y_total[0]+") , center_y_cnt = ("+center_y_cnt[0]+")");
    	return LTP_PM_result;//return LTP_Kois;
    }
    public static List<int[]> LTP_High_Line(Mat mat, int fish_no, int stream, int type, int max_value)
    // 做LTP演算法取256專屬向量編碼，然後輸出LTP藍圖與256個中心坐標跟histogram
    // mat = input image
    // fish_no = 哪一隻魚(魚編號)
    // stream = 目前的串流編號
    // type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
    {
    	int[][][][] P_hist;
    	int[][][][] M_hist;
    	P_hist = new int[10][20][1000][256];
    	M_hist = new int[10][20][1000][256];
//    	byte[] data = {0,0,0};
    	byte[] data_Black = {0,0,0};
//    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
//    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	byte[][] data_LTP;
    	byte[] center_LTP;
    	int data_LTP_P_total;
    	int data_LTP_M_total;
    	Mat dest = new Mat(mat.size(), mat.type());
//    	Mat dest_clean = new Mat(mat.size(), mat.type(),new Scalar(0, 0, 0));
    	for(int k = 0 ; k < 8 ; k ++){P_hist[type][fish_no][stream][k]=0;}
    	List<Mat> LTP_Kois = new ArrayList<Mat>();
    	List<int[]> LTP_PM_result = new ArrayList<int[]>();
    	Mat LTP_P_0 = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	Mat LTP_M = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	LTP_P_0 = mat.clone();
    	ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
    	List<ArrayList<MatOfPoint>> array_contours = new ArrayList<ArrayList<MatOfPoint>>();
    	Mat drawCon = new Mat();
//    	Mat drawCon_mat = new Mat();
    	if(max_value>8){
    		
	    	int contour_step = 0; 
	    	contour_step = max_value / 8 ;
//	    	System.out.println("max_value"+type+" = "+max_value);
//			System.out.println("contour_step"+type+" = "+contour_step);
			for(int i = 0 ; i < max_value ; i = i + contour_step){
				Core.inRange(mat, new Scalar(i, 0, 0), new Scalar(255, 255, 255), dest);
				Imgproc.findContours(dest,
				contours,
				drawCon, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
				array_contours.add(contours) ;
			}
			export(dest, "a_dest");
//			export(dest, "a_dest");
			for(int i = 0; i < 8 ; i++){
				for (int k = 0; k < array_contours.get(i).size(); k++) {
					Imgproc.drawContours(dest, array_contours.get(i), k, new Scalar(255, 255, 255), 1);
				}
			}
			
    	}
    	export(dest, "mat_s"+stream+"_"+fish_no+"_"+type);
    	
    	data_LTP = new byte[8][3];
    	center_LTP = new byte[3];
    	data_LTP_P_total = 0;
    	data_LTP_M_total = 0;
    	long[] center_x_cnt = new long[256];
    	long[] center_y_cnt = new long[256];
    	long[] center_x_total = new long[256];
    	long[] center_y_total = new long[256];
    	for(int k = 0 ; k < 256 ; k ++){
    		center_x_cnt[k]=0;center_y_cnt[k]=0;center_x_total[k]=0;center_y_total[k]=0;
    	}
    	for(int i = 1 ; i < dest.width() -1 ; i ++){
    		for(int j = 1 ; j < dest.height() -1 ; j ++){
    			dest.get(j  , i  , center_LTP);
    			dest.get(j-1, i-1, data_LTP[3]);
    			dest.get(j  , i-1, data_LTP[2]);
    			dest.get(j+1, i-1, data_LTP[1]);
    			dest.get(j-1, i  , data_LTP[4]);
    			dest.get(j+1, i  , data_LTP[0]);
    			dest.get(j-1, i+1, data_LTP[5]);
    			dest.get(j  , i+1, data_LTP[6]);
    			dest.get(j+1, i+1, data_LTP[7]);
    			for(int k = 0 ; k < 8 ; k ++){
//    				if(data_LTP[k][0] < 0){
//        				System.out.println("data_LTP = "+data_LTP[k][0] + " at ( " + i +" , "+ j +" )");
//        			}
	    			if(center_LTP[0] - data_LTP[k][0] >= 0){
	    				data_LTP_P_total = data_LTP_P_total + (int)Math.pow(2 , k);
//	    				for(int l = 0; l < 32 ; l ++)P_hist[type][fish_no][stream][k*32+l]++;
//	    				P_hist[type][fish_no][stream][k]++;
	    			}
	    			if(center_LTP[0] - data_LTP[k][0] < 3){
//	    				M_hist[type][fish_no][stream][k]++;
	    				data_LTP_M_total = data_LTP_M_total + (int)Math.pow(2 , k);
	    			}
//	    			else{
//	    				
//	    			}
    			}
    			P_hist[type][fish_no][stream][data_LTP_P_total]++;
    			M_hist[type][fish_no][stream][data_LTP_M_total]++;
    			byte[] data_P_out = {0,0,(byte) ((data_LTP_P_total))};
    			byte[] data_M_out = {0,0,(byte) ((data_LTP_M_total))};
    			LTP_P_0.put(j, i, data_P_out);
    			LTP_M.put(j, i, data_M_out);
    			data_LTP_P_total = 0;
    			data_LTP_M_total = 0;
    			
        	}
    	}
    	int compare_P = 0;
    	int compare_M = 0;
    	for(int i=1  ; i < 255 ; i++){
    		if(P_hist[type][fish_no][stream][i]>compare_P){compare_P = P_hist[type][fish_no][stream][i];}//(int)P_hist[stream][i];}
    		if(M_hist[type][fish_no][stream][i]>compare_M){compare_M = M_hist[type][fish_no][stream][i];}//(int)M_hist[stream][i];}
    	}
    	Mat LTP_P_hist = new Mat(compare_P, 256, CvType.CV_8UC3);
    	Mat LTP_M_hist = new Mat(compare_M, 256, CvType.CV_8UC3);
    	
		for(int j=1 ; j < 255 ; j++){
			for(int i=0 ; i < compare_P ; i++){
    			if(P_hist[type][fish_no][stream][j]<i){LTP_P_hist.put(compare_P-i, j, data_White);}
    			else{LTP_P_hist.put(compare_P-i, j, data_Black);}
    		}
    	}
		for(int j=1 ; j < 255 ; j++){
			for(int i=1 ; i < compare_M ; i++){
				if(M_hist[type][fish_no][stream][j]<i){LTP_M_hist.put(compare_M-i, j, data_White);}
				else{LTP_M_hist.put(compare_M-i, j, data_Black);}
			}
    	}
		LTP_PM_result.add(P_hist[type][fish_no][stream]);
		LTP_PM_result.add(M_hist[type][fish_no][stream]);
    	LTP_Kois.add(LTP_P_0);
    	LTP_Kois.add(LTP_M);
    	LTP_Kois.add(LTP_P_hist);
    	LTP_Kois.add(LTP_M_hist);
    	
//    	for(int k = 0 ; k < 8 ; k ++){
//    		Imgproc.circle(LTP_P_0,
//	    	new Point(center_x_total[k]/center_x_cnt[k], center_y_total[k]/center_y_cnt[k]), 
//	    	1, 
//	    	new Scalar((32*k), (255-32*k), 255),2);
//	    	export(LTP_P_0, "LTP_P_s"+fish_no+stream+type);
//	    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
//	    }
//    	for(int k = 0 ; k < 256 ; k ++){
//    		if(center_x_cnt[k] > 0 && center_y_cnt[k] > 0){
//    			
//		    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
//    		}
//	    }
//    	System.out.println("center_x_total = ("+center_x_total[0]+") , center_x_cnt = ("+center_x_cnt[0]+")");
//    	System.out.println("center_y_total = ("+center_y_total[0]+") , center_y_cnt = ("+center_y_cnt[0]+")");
    	return LTP_PM_result;
    }
    
    
    public static int[][] CKG(int two_r)
    //circle kernel generator
    {
		int[][] kernel = new int[two_r][two_r];
		int r = two_r/2;
		int temp=0;
		for(int m = 0; m <= r ; m++){
			for(int n = 0 ; n <= r ; n++){
				temp = (int) Math.sqrt((n*n)+(m*m));
				if(temp < r){
					kernel[r+m][r+n] = r - temp;
					kernel[r-m][r+n] = r - temp;
					kernel[r+m][r-n] = r - temp;
					kernel[r-m][r-n] = r - temp;
				}
				else{
					kernel[r+m][r+n] = 0;
					kernel[r-m][r+n] = 0;
					kernel[r+m][r-n] = 0;
					kernel[r-m][r-n] = 0;
				}
			}
		}
		return kernel;
    }
    
    
    public static List<int[]> Drops_High_Line(Mat mat, int fish_no, int stream, int type, int max_value)
    // 做LTP演算法取256專屬向量編碼，然後輸出LTP藍圖與256個中心坐標跟histogram
    // mat = input image
    // fish_no = 哪一隻魚(魚編號)
    // stream = 目前的串流編號
    // type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
    {
    	int[][][][] P_hist;
    	int[][][][] M_hist;
    	P_hist = new int[10][20][1000][256];
    	M_hist = new int[10][20][1000][256];
    	byte[] data = {0,0,0};
    	byte[] data_line = {0,0,0};
    	
    	byte[] data1 = {0,0,0};
    	byte[] data2 = {0,0,0};
    	byte[] data3 = {0,0,0};
    	byte[] data4 = {0,0,0};
//    	byte[] data_Black = {0,0,0};
//    	byte[] data_Gary = {(byte) 180,(byte) 180,(byte) 180};
//    	byte[] data_Background = {(byte) 255,(byte) 255,0};
//    	byte[] data_Blue = {(byte) 255,0,0};
//    	byte[] data_Green = {0,(byte) 255,0};
//    	byte[] data_Red = {0,0,(byte) 255};
//    	byte[] data_White = {(byte) 255,(byte) 255,(byte) 255};
    	byte[][] data_LTP;
    	byte[] center_LTP;
    	int data_LTP_P_total;
    	int data_LTP_M_total;
    	Mat dest = new Mat(mat.size(), mat.type());
//    	Mat draw_dest = new Mat(mat.size(), mat.type());
//    	Mat dest_clean = new Mat(mat.size(), mat.type(),new Scalar(0, 0, 0));
    	for(int k = 0 ; k < 8 ; k ++){P_hist[type][fish_no][stream][k]=0;}
    	List<Mat> LTP_Kois = new ArrayList<Mat>();
    	List<int[]> LTP_PM_result = new ArrayList<int[]>();
    	Mat LTP_P_0 = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	Mat LTP_M = new Mat(mat.height(), mat.width(), CvType.CV_8UC3);
    	Mat Drops_High_Line = Mat.zeros(mat.height(), mat.width(), CvType.CV_8UC3);
    	LTP_P_0 = mat.clone();
    	
//    	List<ArrayList<MatOfPoint>> array_contours = new ArrayList<ArrayList<MatOfPoint>>();
    	Mat drawCon = new Mat();
//    	Mat drawCon_mat = new Mat();
//    	Core.inRange(draw_dest, new Scalar(255, 255, 255), new Scalar(0, 0, 0), draw_dest);
    	int[][] circle_kernel;
    	circle_kernel = CKG(32+32+1);
    	if(max_value > 8){
    		
	    	int contour_step = 0; 
	    	contour_step = max_value / 8 ;
//	    	System.out.println("max_value"+type+" = "+max_value);
//			System.out.println("contour_step"+type+" = "+contour_step);
	    	export(mat, "a_dest_be");
	    	int cnt=0;
//	    	int contour_cnt=0;
			for(int i = contour_step ; i <= max_value; i = i + contour_step){
				Mat draw_dest = Mat.zeros(mat.size(), mat.type());
				ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
				Core.inRange(mat, new Scalar(i, 0, 0), new Scalar(255, 0, 0), dest);
//				export(dest, "/"+stream+"_"+type+"/a_dest_"+cnt);
				Imgproc.findContours(dest,
				contours,
				drawCon, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
//				array_contours.add(contours) ;
				for (int k = 0; k < contours.size(); k++) {
					Imgproc.drawContours(draw_dest, 
					contours, 
					k, new Scalar(255, 255, 255), 1);
				}
				
				cnt++;
				for(int k = 0 ; k < draw_dest.width() ; k ++){
		    		for(int j = 0 ; j < draw_dest.height() ; j ++){
		    			draw_dest.get(j , k , data);
		    			if(data[2]==(byte)255){
		    				for(int m = 0; m < 32 ; m++){
		    					for(int n = 0; n < 32 ; n++){
		    						if(k - n >= 0 && j - m >=0 && k + n < draw_dest.width() && j + m <draw_dest.height()){
			    						draw_dest.get(j - m , k - n , data1);
			    						draw_dest.get(j - m , k + n , data2);
			    						draw_dest.get(j + m , k - n , data3);
			    						draw_dest.get(j + m , k + n , data4);
			    						if(data1[0] < circle_kernel[31 - m][31 - n]){
			    							data1[0] = (byte)circle_kernel[31 - m][31 - n];
			    						}
			    						if(data2[0] < circle_kernel[31 - m][31 + n]){
			    							data2[0] = (byte)circle_kernel[31 - m][31 + n];
			    						}
			    						if(data3[0] < circle_kernel[31 + m][31 - n]){
			    							data3[0] = (byte)circle_kernel[31 + m][31 - n];
			    						}
			    						if(data4[0] < circle_kernel[31 + m][31 + n]){
			    							data4[0] = (byte)circle_kernel[31 + m][31 + n];
			    						}
			    						draw_dest.put(j - m , k - n, data1);
			    						draw_dest.put(j - m , k + n, data2);
			    						draw_dest.put(j + m , k - n, data3);
			    						draw_dest.put(j + m , k + n, data4);
		    						}
			    				}	
		    				}
		    				data1[0] = (byte)32;
		    				data1[1] = 0;
		    				data1[2] = 0;
		    				draw_dest.put(j , k , data1);
		    			}
		    		}
				}
				export(draw_dest, "/"+stream+"_"+type+"/a_dest_"+cnt+100);
				for(int k = 0 ; k < draw_dest.width() ; k ++){
		    		for(int j = 0 ; j < draw_dest.height() ; j ++){
		    			draw_dest.get(j, k, data);
		    			Drops_High_Line.get(j, k, data_line);
		    			data_line[0] = (byte) (data_line[0] + data[0]);
		    			Drops_High_Line.put(j, k, data_line);
		    		}
		    	}
//				Drops_High_Line = Drops_High_Line.draw_dest;
//				Core.inRange(Drops_High_Line, new Scalar(1 ,0 ,0), new Scalar((byte)255 ,(byte)255 ,(byte)255), Drops_High_Line);
//				Imgproc.threshold(Drops_High_Line, Drops_High_Line, 1, 255, Imgproc.THRESH_OTSU);
				export(Drops_High_Line, "/"+stream+"_"+type+"/a_dest_"+cnt);
			}
			System.out.println("stream = "+stream + " , type = " +type);
//			export(dest, "a_dest");
			
    	}
    	export(dest, "mat_s"+stream+"_"+fish_no+"_"+type);
    	
    	data_LTP = new byte[8][3];
    	center_LTP = new byte[3];
    	data_LTP_P_total = 0;
    	data_LTP_M_total = 0;
    	long[] center_x_cnt = new long[256];
    	long[] center_y_cnt = new long[256];
    	long[] center_x_total = new long[256];
    	long[] center_y_total = new long[256];
    	for(int k = 0 ; k < 256 ; k ++){
    		center_x_cnt[k]=0;center_y_cnt[k]=0;center_x_total[k]=0;center_y_total[k]=0;
    	}
    	for(int i = 1 ; i < dest.width() -1 ; i ++){
    		for(int j = 1 ; j < dest.height() -1 ; j ++){
    			dest.get(j  , i  , center_LTP);
    			dest.get(j-1, i-1, data_LTP[3]);
    			dest.get(j  , i-1, data_LTP[2]);
    			dest.get(j+1, i-1, data_LTP[1]);
    			dest.get(j-1, i  , data_LTP[4]);
    			dest.get(j+1, i  , data_LTP[0]);
    			dest.get(j-1, i+1, data_LTP[5]);
    			dest.get(j  , i+1, data_LTP[6]);
    			dest.get(j+1, i+1, data_LTP[7]);
    			for(int k = 0 ; k < 8 ; k ++){
//    				if(data_LTP[k][0] < 0){
//        				System.out.println("data_LTP = "+data_LTP[k][0] + " at ( " + i +" , "+ j +" )");
//        			}
	    			if(center_LTP[0] - data_LTP[k][0] >= 0){
	    				data_LTP_P_total = data_LTP_P_total + (int)Math.pow(2 , k);
//	    				for(int l = 0; l < 32 ; l ++)P_hist[type][fish_no][stream][k*32+l]++;
//	    				P_hist[type][fish_no][stream][k]++;
	    			}
	    			if(center_LTP[0] - data_LTP[k][0] < 3){
//	    				M_hist[type][fish_no][stream][k]++;
	    				data_LTP_M_total = data_LTP_M_total + (int)Math.pow(2 , k);
	    			}
//	    			else{
//	    				
//	    			}
    			}
    			P_hist[type][fish_no][stream][data_LTP_P_total]++;
    			M_hist[type][fish_no][stream][data_LTP_M_total]++;
    			byte[] data_P_out = {0,0,(byte) ((data_LTP_P_total))};
    			byte[] data_M_out = {0,0,(byte) ((data_LTP_M_total))};
    			LTP_P_0.put(j, i, data_P_out);
    			LTP_M.put(j, i, data_M_out);
    			data_LTP_P_total = 0;
    			data_LTP_M_total = 0;
    			
        	}
    	}
    	int compare_P = 0;
    	int compare_M = 0;
    	for(int i=1  ; i < 255 ; i++){
    		if(P_hist[type][fish_no][stream][i]>compare_P){compare_P = P_hist[type][fish_no][stream][i];}//(int)P_hist[stream][i];}
    		if(M_hist[type][fish_no][stream][i]>compare_M){compare_M = M_hist[type][fish_no][stream][i];}//(int)M_hist[stream][i];}
    	}
    	Mat LTP_P_hist = new Mat(compare_P, 256, CvType.CV_8UC3);
    	Mat LTP_M_hist = new Mat(compare_M, 256, CvType.CV_8UC3);
    	
		LTP_PM_result.add(P_hist[type][fish_no][stream]);
		LTP_PM_result.add(M_hist[type][fish_no][stream]);
    	LTP_Kois.add(LTP_P_0);
    	LTP_Kois.add(LTP_M);
    	LTP_Kois.add(LTP_P_hist);
    	LTP_Kois.add(LTP_M_hist);
    	
//    	for(int k = 0 ; k < 8 ; k ++){
//    		Imgproc.circle(LTP_P_0,
//	    	new Point(center_x_total[k]/center_x_cnt[k], center_y_total[k]/center_y_cnt[k]), 
//	    	1, 
//	    	new Scalar((32*k), (255-32*k), 255),2);
//	    	export(LTP_P_0, "LTP_P_s"+fish_no+stream+type);
//	    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
//	    }
//    	for(int k = 0 ; k < 256 ; k ++){
//    		if(center_x_cnt[k] > 0 && center_y_cnt[k] > 0){
//    			
//		    	System.out.println("center(x"+k+",y"+k+") = ("+center_x_total[k]/center_x_cnt[k]+" , "+center_y_total[k]/center_y_cnt[k]+")");
//    		}
//	    }
//    	System.out.println("center_x_total = ("+center_x_total[0]+") , center_x_cnt = ("+center_x_cnt[0]+")");
//    	System.out.println("center_y_total = ("+center_y_total[0]+") , center_y_cnt = ("+center_y_cnt[0]+")");
    	return LTP_PM_result;
    }
    //TODO
    public static List<Mat> WK_QR(Mat mat, int fish_no, int stream, int type, int Y_th, int Yth)
    // 針對白寫做辨識
	// (1)原圖的二值化 (2)分網格 (3)轉魚身 (4)QR code (5)比例含量累加
    // mat = 原圖
	// fish_no = 哪一隻魚(魚編號)
	// stream = 目前的串流編號
	// type = 目前在做LTP的是哪個模組(contour_R = 0  contour_WK = 1)
	{
//    	List<int[]> WK_id_result = new ArrayList<int[]>();
//    	Mat Bi_mat = new Mat();

    	List<Mat> stand_get_mat_list = new ArrayList<Mat>();
    	
    	Mat mask_mat = new Mat();
    	int Size_y = 0;
    	if((mat.height()/4) > 4){Size_y = (mat.height()/4) ;}else{Size_y = 1;}
    	Mat kernel_wing = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1,Size_y));
//    	Mat kernel = new Mat((mat.height()/4), 1, CvType.CV_8UC1);
    	Mat re_get_mat = mat.clone();
    	Mat stand_get_mat = new Mat(re_get_mat.size(), re_get_mat.type());
    	
    	Mat stand_get_mat_R = new Mat(re_get_mat.size(), CvType.CV_8UC1);
    	Mat re_get_mat_R = new Mat(re_get_mat.size(), CvType.CV_8UC1);
    	
    	Mat stand_get_mat_W = new Mat(re_get_mat.size(), CvType.CV_8UC1);
    	
    	Rect wing_erodila_size = null ;
    	Rect wing_erodila_size_temp = null ;
    	List<MatOfPoint> contour_wing = new ArrayList<MatOfPoint>();
    	Mat drawCon = new Mat();
    	int[] x_width  = new int[9];
    	int[] y_height = new int[33];
//    	int[] QR_code = new int[256];
//    	int stand_size_min = mat.width();
//		int stand_size_max = 0;
    	
    	//原圖(單獨一隻切好的魚)
    	export(mat, "fish");
    	
    	//原圖二值化去掉背景
    	Core.inRange(re_get_mat, new Scalar(0, 0, 1), new Scalar(255, 255, 255), re_get_mat);
    	export(re_get_mat, "re_get_fish");
    	
    	//原圖二值化去掉兩側魚鰭
    	Imgproc.erode(re_get_mat, re_get_mat, kernel_wing);
    	Imgproc.dilate(re_get_mat, re_get_mat, kernel_wing);
    	export(re_get_mat, "erodila_re_get_fish_wing");//Yth
    	

    	List<Mat> segRed_result = new ArrayList<Mat>();
    	segmentation segRed = new segmentation();
    	segRed.segRed(mat, segRed_result);

    	export(segRed_result.get(0), "segRed_result.get(0)"+stream+"_"+fish_no);
    	export(segRed_result.get(1), "segRed_result.get(1)"+stream+"_"+fish_no);
    	Core.subtract(re_get_mat, segRed_result.get(0), re_get_mat_R);

    	export(re_get_mat_R, "re_get_mat_segRed_result.get(0)_"+stream+"_"+fish_no);
    	
    	Mat mat_SML = Mat.zeros(re_get_mat.size(),CvType.CV_8UC1);
//    	Mat mat_SML_color = new Mat(re_get_mat.size(),CvType.CV_8UC3);
    	Mat mat_SML_color = Mat.zeros(re_get_mat.size(),CvType.CV_8UC3);
    	Mat dest_SML = new Mat();
    	Core.inRange(re_get_mat_R, new Scalar(1, 1, 1), new Scalar(255, 255, 255), dest_SML);
    	
    	mat.copyTo(mat_SML_color, dest_SML);
    	export(mat_SML_color, "before_blue_restore"+stream+"_"+fish_no);
//    	System.out.println("blue_restore");
    	blue_restore(mat_SML_color,mat_SML_color);
//    	Imgproc.equalizeHist(mat_SML_color, mat_SML_color);
    	export(mat_SML_color, "blue_restore"+stream+"_"+fish_no);
//    	System.out.println("SML_filter");
    	SML_filter(mat_SML_color,mat_SML,fish_no,stream);
    	export(mat_SML, "mat_SML"+stream+"_"+fish_no);
//    	System.out.println("SML_filter end");

    	//取得去掉魚鰭範圍的框框
    	Imgproc.findContours(re_get_mat, contour_wing, drawCon, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
    	wing_erodila_size = Imgproc.boundingRect(contour_wing.get(0));
    	for(int i = 0 ; i < contour_wing.size() ; i ++){
    		
    		wing_erodila_size_temp = Imgproc.boundingRect(contour_wing.get(i));
    		if(wing_erodila_size.area() <= wing_erodila_size_temp.area()){
    			wing_erodila_size = Imgproc.boundingRect(contour_wing.get(i));
    		}
    	}
    	
    	Core.inRange(re_get_mat, new Scalar(1, 1, 1), new Scalar(255, 255, 255), re_get_mat);
    	
    	
    	//取得格線XY座標
    	for(int i = 0 ; i <= 8 ; i++){x_width[i] = (wing_erodila_size.width*(i))/8; }
    	for(int i = 0 ; i <= 32 ; i++){y_height[i] = (wing_erodila_size.height*(i))/32; }
    	
    	mask_mat = re_get_mat;
    	
    	Mat stand_mat = new Mat(re_get_mat.size(), mat.type());
    	Mat stand_bi_mat = new Mat(re_get_mat.size(), re_get_mat.type());
//    	Mat x_mat = new Mat(1,wing_erodila_size.width, stand_mat.type());
//    	System.out.println("erodila_size");
    	for(int m = 0 ; m < re_get_mat.height() ; m++){
    		int erodila_size_min = re_get_mat.width();
			int erodila_size_max = 0;
			int erodila_size_mid = 0;
			int erodila_size_r = 0;
			int erodila_size_l = 0; 
    		for(int n = 0 ; n < re_get_mat.width() ; n++){
    			if(((int)re_get_mat.get(m, n)[0]) > 0){
	    			if(erodila_size_min > n){erodila_size_min = n;}
	    			if(erodila_size_max < n){erodila_size_max = n;}
    			}
    		}

    		if(erodila_size_max-erodila_size_min >0){
    			erodila_size_l = erodila_size_max-erodila_size_min;
	    		Mat x_before_mat = new Mat(1,erodila_size_l, CvType.CV_8UC1);
	    		erodila_size_r = (erodila_size_max-erodila_size_min)/2;
	    		erodila_size_mid = (wing_erodila_size.width/2) + wing_erodila_size.x;
	    		for(int n = 0 ; n < re_get_mat.width() ; n++){
	    			if(n >= erodila_size_mid - erodila_size_r && n <= erodila_size_mid + erodila_size_r ){
	    				byte[] data_White = {(byte)255,(byte)255,(byte)255};
	    				stand_mat.put(m, n, mat.get(m, (erodila_size_min + (n -  (erodila_size_mid - erodila_size_r)))));
	    				stand_bi_mat.put(m, n, data_White);
	    				stand_get_mat_R.put(m, n, segRed_result.get(0).get(m, (erodila_size_min + (n -  (erodila_size_mid - erodila_size_r)))));
	    				stand_get_mat_W.put(m, n, mat_SML.get(m, (erodila_size_min + (n -  (erodila_size_mid - erodila_size_r)))));
	    				x_before_mat.put(0, n-(erodila_size_mid - erodila_size_r), mask_mat.get(m, (erodila_size_min + (n -  (erodila_size_mid - erodila_size_r)))));
	    			}
	    			else{
	    				byte[] data_Black = {0,0,0};
	    				byte[] data_Black_1 = {0};
	    				stand_mat.put(m, n, data_Black);
	    				stand_bi_mat.put(m, n, data_Black);
	    				stand_get_mat_R.put(m, n, data_Black_1);
	    				stand_get_mat_W.put(m, n, data_Black_1);
	    				
	    			}
	    		}
//	    		export(stand_get_mat, "stand_get_mat");
    		}
    		else{

    			for(int n = 0 ; n < re_get_mat.width() ; n++){
    				byte[] data_Black = {0,0,0};
    				byte[] data_Black_1 = {0};
    				stand_get_mat_R.put(m, n, data_Black_1);
    				stand_get_mat_W.put(m, n, data_Black_1);
    				stand_mat.put(m, n, data_Black);
    				stand_bi_mat.put(m, n, data_Black);
    				stand_get_mat.put(m, n, data_Black);
	    		}
    		}
    		if(m > 50 && m <100){
//    		export(x_before_mat, "x_before_mat"+m);
//    		export(x_mat, "x_mat"+m);
    		}
    		
    	}
//    	export(mask_mat, "re_get_mask");//Yth
    	export(re_get_mat, "re_get_mat"+stream+"_"+fish_no);
    	export(stand_get_mat_R, "stand_get_mat_R");
    	export(stand_get_mat_W, "stand_get_mat_W");
    	export(stand_mat, "stand_mat_"+stream+"_"+fish_no);//Yth
    	
		List<MatOfPoint> contours_pre_WK = new ArrayList<MatOfPoint>();
		Mat dest_WK = new Mat();
		Mat drawCon_WK = new Mat();
		Mat re_get_mat_bigone = new Mat();
    	Core.inRange(re_get_mat, new Scalar(1, 1, 1), new Scalar(255, 255, 255), dest_WK);
		Imgproc.findContours(dest_WK, contours_pre_WK, drawCon_WK, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
	
		//取得最大contour的contour編號與大小
		int max_area_temp = 0;
		int max_area_pre_WK = 0;
		
		for(int i=0; i< contours_pre_WK.size();i++){
			Rect rect = Imgproc.boundingRect(contours_pre_WK.get(i));
		    if((int)rect.area() > max_area_temp){
		    	max_area_pre_WK = i;
		    	max_area_temp= (int) rect.area();
		    } 
		}
		Imgproc.drawContours(re_get_mat_bigone, contours_pre_WK, max_area_pre_WK, new Scalar(255), Core.FILLED);
		Rect rect = Imgproc.boundingRect(contours_pre_WK.get(max_area_pre_WK));
		Mat stand_get_mat_R_recontour_big = new Mat(rect.size(), CvType.CV_8UC1);
		Mat stand_get_mat_W_recontour_big = new Mat(rect.size(), CvType.CV_8UC1);
		stand_get_mat_R_recontour_big = stand_get_mat_R.submat(rect);
		stand_get_mat_W_recontour_big = stand_get_mat_W.submat(rect);

    	export(stand_get_mat_R_recontour_big, "stand_get_mat_R_recontour_big_"+stream+"_"+fish_no);
    	export(stand_get_mat_W_recontour_big, "stand_get_mat_W_recontour_big_"+stream+"_"+fish_no);
    	
//    	export(stand_bi_mat, "stand_bi_mat");//Yth
//    	WK_id_result.add(QR_code);
    	stand_get_mat_list.add(stand_get_mat_R_recontour_big);
    	stand_get_mat_list.add(stand_get_mat_W_recontour_big);
		return stand_get_mat_list;//return LTP_Kois;
	}
    
    public static void SML_filter(Mat src_color, Mat dst_gray,int fish,int stream){
    	
    	double[] S_weight = new double[256];//低通
//    	double[] M_weight = new double[256];//帶通
//    	double[] L_weight = new double[256];//高通
    	double[][] temp = new double[9][];
    	double data = 0.0 ;
    	
    	Mat Y_mat = new Mat(src_color.size(),CvType.CV_8UC1);
    	Mat CAn_mat = new Mat(src_color.size(),CvType.CV_8UC1);
    	Mat calcHis = new Mat();
    	MatOfFloat ranges=new MatOfFloat(0,255);
    	MatOfInt hstSize = new MatOfInt(255);
    	
    	for(int i = 0 ; i <256; i++ ){
    		if( i <= 64 && i >= 1 ) S_weight[i] = 1 ; else if( i <= 128 && i >= 64 ) S_weight[i] = (128-(double)i)/64; else S_weight[i] = 0 ;
//    		if( i <= 128 && i >= 85 )	M_weight[i] = ((double)i-85)/43 ; else if( i <= 170 && i >= 128 ) M_weight[i] = (171-(double)i)/43 ; else M_weight[i] = 0 ;
//    		if( i <= 213 && i >= 170 ) L_weight[i] = ((double)i-170)/43 ; else if( i >= 213 ) L_weight[i] = (256 - (double)i)/43 ; else L_weight[i] = 0 ;
    	}
    	
    	List<Mat> matList = new ArrayList<Mat>();
    	
    	Imgproc.cvtColor(src_color,Y_mat,Imgproc.COLOR_BGR2GRAY);
    	
    	
    	Core.normalize(Y_mat, Y_mat,0, 255,Core.NORM_MINMAX, -1);
    	export(Y_mat, "Y_mat"+ fish+"_"+stream);
    	
    	matList.add(Y_mat);
    	
    	Imgproc.calcHist(	matList, 
	    	                new MatOfInt(0), 
	    	                new Mat(), 	
	    	                calcHis , 
	    	                hstSize , 
	    	                ranges);
    	
    	for(int i = 1 ; i < Y_mat.width()-1 ; i++){
    		for(int j = 1 ; j < Y_mat.height()-1 ; j++){
   				temp[0] = Y_mat.get(j-1,i-1 );
   				temp[1] = Y_mat.get(j-1,i );
   				temp[2] = Y_mat.get(j-1,i+1 );
   				temp[3] = Y_mat.get(j,i-1 );
   				temp[4] = Y_mat.get(j,i );
   				temp[5] = Y_mat.get(j,i+1 );
   				temp[6] = Y_mat.get(j+1,i-1 );
   				temp[7] = Y_mat.get(j+1,i );
   				temp[8] = Y_mat.get(j+1,i+1 );
   				data = SML_kernel_calc(temp,S_weight);
   				byte[] data_White = {(byte)255};
   				byte[] data_Black = {0};
   				if(data > 7.2){
   					dst_gray.put(j,i, data_White);
    			}
    			else{
    				dst_gray.put(j,i, data_Black);
    			}
    		}
		}
    	for(int i = 0 ; i < dst_gray.width() ; i++){
    		for(int j = 0 ; j < dst_gray.height() ; j++){

    			byte[] data_Black = {(byte)0};
//    			byte[] data_White = {(byte)255,(byte)255,(byte)255};
    			if(i == 0 || i == (dst_gray.width()-1) || j == 0 || j == (dst_gray.height()-1)){
    				CAn_mat.put(j,i, data_Black);
    			}
				else{
					dst_gray.get(j,i,data_Black);
					CAn_mat.put(j,i, data_Black);
				}
    		}
    	}
    	export(CAn_mat, "CAn_mat" + cnt);
    	dst_gray = CAn_mat.clone();
    }
    
    public static double SML_kernel_calc(double[][] temp, double[] weight){
    	//SML_kernel_calc 負責加權平均的運算
    	//輸入[n*n]的陣列
    	//輸入權重對應表 weight[](0~255)
    	//輸入n(size)
    	//輸出加權平均後的結果
    	double result = 0.0;
    	//n*n的迴圈
    	for(int i = 0 ; i < temp.length ; i++){
    		//累加權重數
    		result = result + weight[(int)temp[i][0]];
    	}
    	//輸出加權平均後的結果
    	return result;///temp_result;
    }
    
    public static double get_most_like(double src, double A, double B, double C){
    	//取 A、B、C 中最接近 Src 的値
    	double src_A = Math.abs(src - A);
    	double src_B = Math.abs(src - B);
    	double src_C = Math.abs(src - C);
    	if(src_A <= src_B && src_A <= src_C)
    	return A;
    	else if(src_B <= src_A && src_B <= src_C)
    	return B;
    	else if(src_C <= src_A && src_C <= src_B)
    	return C;
    	else
    	return 100;	
    }
    
    public static void blue_restore(Mat src, Mat dst){
    	//藍色補償
    	//將錦鯉邊緣的藍色補償
    	double[] data = {0,0,0};
    	for(int i = 0 ; i < src.width() ; i++){
    		for(int j = 0 ; j < src.height() ; j++){
    			double rgb_R = src.get(j, i)[2];
    			double rgb_G = src.get(j, i)[1];
    			double rgb_B = src.get(j, i)[0];
    			double B_G = rgb_B - rgb_G ;
    			double B_R = rgb_B - rgb_R ;
    			double Big_distance = 0;
    			if(B_G > 0 || B_R > 0){
    				if(B_G > B_R)	Big_distance = B_G;
    				else Big_distance = B_R;
    				data[0] = rgb_B - Big_distance/6 + Big_distance/6;
    				data[1] = rgb_G + Big_distance/3 + Big_distance/6;
    				data[2] = rgb_R + Big_distance*2/3 + Big_distance/6;
    				dst.put(j, i, data);
    			}
    			else{
    				dst.put(j, i, src.get(j, i));
    			}
    		}
    	}
    }
}