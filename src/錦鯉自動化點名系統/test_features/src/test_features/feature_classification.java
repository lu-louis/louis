package test_features;

import java.util.ArrayList;
import java.util.Arrays;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class feature_classification {

	//all_FeaturesParam_json         所有特徵參數
	//all_FeaturesParam_cut_lenght   所有特徵參數總數
	//kind                           預分出的類別數
	 public void classification(JSONObject all_FeaturesParam_json ,int all_FeaturesParam_cut_lenght,int kind){ 

		 
		 System.out.println("共切出錦鯉數: "+all_FeaturesParam_cut_lenght);
		 System.out.println("預分出的類別數: "+kind);
		 
		 ArrayList<ArrayList<JSONObject>>category = new ArrayList<ArrayList<JSONObject>>();   //預設類別 之後會慢慢遞增
		 
		 for(int i=1 ;i<=all_FeaturesParam_cut_lenght;i++)
		 {
			 if(i==1)           //第一次將第一個切出的特徵當成1個類別
			 {
				 
				 category.add(new ArrayList());        //add   category[0]
				 
				 try {
					 category.get(0).add(all_FeaturesParam_json.getJSONObject("koifeaturesParam_0"));
					 System.out.println("koifeaturesParam_0: "+all_FeaturesParam_json.getJSONObject("koifeaturesParam_0"));
					 System.out.println("category[0][0]: "+category.get(0).get(0));
				 } catch (JSONException e) {
					 // TODO Auto-generated catch block
			         e.printStackTrace();
		         }
				 
			 }
			 else
			 {
				 System.out.println("第: "+i + "隻===============================================================");
				 float[] Difference_averages = new float[category.size()];    //各類別比較後差異值總和
				 float[] Difference_averages_reg = new float[category.size()];
				 			 
				 //將當前要比對的 特徵參數   json 轉 int[]=============================================================================
	    		 int [] sd1_i=new int[8];
	    		 int [] sc1_i=new int[254];
	    		 int [] sc2_i=new int[254];
	    		 int [] sc3_i=new int[254];
	    		 
	    		try {
	    			 
	    		 for(int o=0;o<sd1_i.length;o++)
	    		 {

			    	 sd1_i[o]=all_FeaturesParam_json.getJSONObject("koifeaturesParam_"+(i-1)).getJSONObject("param").getJSONArray("sd1").getInt(o);
			    			 
	    		 }
	    		 
	    		 for(int o=0;o<sc1_i.length;o++)
	    		 {

	    			 sc1_i[o]=all_FeaturesParam_json.getJSONObject("koifeaturesParam_"+(i-1)).getJSONObject("param").getJSONArray("sc1").getInt(o);
	    			 
	    		 }
	    		 
	    		 for(int o=0;o<sc2_i.length;o++)
	    		 {

	    			 sc2_i[o]=all_FeaturesParam_json.getJSONObject("koifeaturesParam_"+(i-1)).getJSONObject("param").getJSONArray("sc2").getInt(o);
	    			 
	    		 }
	    		 
	    		 for(int o=0;o<sc3_i.length;o++)
	    		 {

	    			 sc3_i[o]=all_FeaturesParam_json.getJSONObject("koifeaturesParam_"+(i-1)).getJSONObject("param").getJSONArray("sc3").getInt(o);
	    			 
	    		 }
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		 
	    		 //=========================================================================================
	    		 
				 
			     for(int category_count=0;category_count<category.size();category_count++)          //類別
			     {
			    	 for(int cut_count=0;cut_count<category.get(category_count).size();cut_count++) //類別內切出內容
			    	 {
			    		 //json 轉 int[]=============================================================================
			    		 int [] sd1_cut=new int[8];
			    		 int [] sc1_cut=new int[254];
			    		 int [] sc2_cut=new int[254];
			    		 int [] sc3_cut=new int[254];
			    		 
			    		try {

			    		for(int l=0;l<i;l++)
			    		{
			    		  if(category.get(category_count).get(cut_count).getJSONObject("param")!=null)
			    		  {	 
			    		     for(int o=0;o<sd1_cut.length;o++)
			    		     {

					    			 sd1_cut[o]=category.get(category_count).get(cut_count).getJSONObject("param").getJSONArray("sd1").getInt(o);
			    		
			    		     }
			    		     
			    		     for(int o=0;o<sc1_cut.length;o++)
			    		     {

			    			         sc1_cut[o]=category.get(category_count).get(cut_count).getJSONObject("param").getJSONArray("sc1").getInt(o);

			    		     }
			    		 
			    		     for(int o=0;o<sc2_cut.length;o++)
			    		     {
			    			 
			    			         sc2_cut[o]=category.get(category_count).get(cut_count).getJSONObject("param").getJSONArray("sc2").getInt(o);

			    		     }
			    		 
			    		     for(int o=0;o<sc3_cut.length;o++)
			    		     {
			    			 
			                            sc3_cut[o]=category.get(category_count).get(cut_count).getJSONObject("param").getJSONArray("sc3").getInt(o);

			    		     }
			    		  }
			    		} 
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    		 //=========================================================================================
			    		 
			    		 
			    		 //特徵比對
			    		 feature_comparison fc=new feature_comparison();
			    		 int Difference = fc.classification(sd1_i,sc1_i,sc2_i,sc3_i,sd1_cut,sc1_cut,sc2_cut,sc3_cut);
			    		 System.out.println("Difference: "+Difference);
			    		 
//			    		 System.out.println("第: "+i + "隻===============================================================");
			    		 System.out.println("**********************************************************");
			    		 
			    		 
			    		 if(Difference==1)   //差異大+ 1/類別內總數
			    		 {	 
			    		 Difference_averages[category_count]=Difference_averages[category_count]+1/category.get(category_count).size();
			    		 Difference_averages_reg[category_count]=Difference_averages_reg[category_count]+1/category.get(category_count).size();
			    		 }
			    	 }
				 
			     }
			     
			     
			     Arrays.sort(Difference_averages);
			     
			     
				 System.out.println("Difference_averages: "+Difference_averages[0]);	//最小的數  最接近  差異最小

				 if(Difference_averages[0]<=0.5)   //平均值小於0.5  差異小
				 {
					 for(int n=0;n<Difference_averages_reg.length;n++)   //找出它原本的類別並將其加入
					 {
						 if(Difference_averages[0]==Difference_averages_reg[n])
						 {	 
							try {
								category.get(n).add(all_FeaturesParam_json.getJSONObject("koifeaturesParam_"+(i-1)));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						 }	
					 }
					 
				 }
				 else  //差異大  增加一個類別 並將其加入
				 {
					 category.add(new ArrayList());   //增加一個類別
					 
				     try {
						category.get(Difference_averages_reg.length).add(all_FeaturesParam_json.getJSONObject("koifeaturesParam_"+(i-1)));
	 				 } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 }
						
				 }
			     
			     
			     
			     
			 }
			

		 }
		 


	 }
}
